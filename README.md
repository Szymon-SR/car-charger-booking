Web app for viewing and booking electric car chargers.

React + .NET

# Backend

Dockerized instruction: (only works for http for now...)
to Run docker:
1:

docker build -t chargers .

2:

docker-compose up

then CTRL + C to stop containers

chargers app url:
http://localhost:8001/

chargers app swagger url:
http://localhost:8001/swagger/index.html

chargers db url:
http://localhost:8002/

# Frontend

Running frontend without docker:

- cd frontend_react
- npm install
- npm run dev
- visit http://localhost:8000/

Running frontend in docker:

- cd frontend_react
- docker-compose build
- docker-compose up
- visit http://172.18.0.2:8000/

dotnet dev-certs https -ep $env:USERPROFILE\.apsnet\https\backend_dotnet.pfx -p pa55w0rd!
dotnet dev-certs https --trust

dotnet user-secrets set "Kestrel:Certificates:Development:Password" "pa55w0rd!"
pa55w0rd!
