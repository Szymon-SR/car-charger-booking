using backend_dotnet.Models;

namespace backend_dotnet.Repositories
{
    public interface IChargingStationRepository
    {
        Task<IEnumerable<ChargingStation>> GetChargingStations();
        Task<ChargingStation> GetChargingStation(int id);
        Task<ChargingStation> AddChargingStation(ChargingStation ChargingStation);
        Task<ChargingStation> UpdateChargingStation(ChargingStation ChargingStation);
        Task<ChargingStation> DeleteChargingStation(int id);
    }
}