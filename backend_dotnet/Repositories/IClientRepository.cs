using backend_dotnet.Models;

namespace backend_dotnet.Repositories
{
    public interface IClientRepository
    {
        Task<IEnumerable<Client>> GetClients();
        Task<Client> GetClient(int id);
        Task<Client> AddClient(Client Client);
        Task<Client> UpdateClient(Client Client);
        Task<Client> DeleteClient(int id);

        Task<Client> GetClientByEmail(string email);
    }
}