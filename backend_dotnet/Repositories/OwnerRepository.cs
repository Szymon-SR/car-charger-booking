﻿using backend_dotnet.Data;
using backend_dotnet.Models;
using Microsoft.EntityFrameworkCore;

namespace backend_dotnet.Repositories
{
    public class OwnerRepository : IOwnerRepository
    {

        private readonly ChargingStationContext _context;
        public OwnerRepository(ChargingStationContext context)
        {
            _context = context;
        }

        public async Task<Owner> AddOwner(Owner Owner)
        {
            var result = await _context.Owners.AddAsync(Owner);
            await _context.SaveChangesAsync();
            return result.Entity;
        }


        public async Task<Owner> DeleteOwner(int id)
        {
            var Owner = await _context.Owners.FindAsync(id);
            if (Owner == null)
                return null;


            _context.Owners.Remove(Owner);
            await _context.SaveChangesAsync();
            return Owner;
        }

        public async Task<Owner> GetOwner(int id)
        {
            // find with by email and include charging stations and addresses
            var Owner = await _context.Owners
                .Include(o => o.ChargingStations)
                .ThenInclude(c => c.Address)
                .FirstOrDefaultAsync(o => o.Id == id);
            return Owner;
        }

        public async Task<Owner> GetOwnerByEmail(string email)
        {
            // find with by email and include charging stations and addresses
            var Owner = await _context.Owners
                .Include(o => o.ChargingStations)
                .ThenInclude(c => c.Address)
                .FirstOrDefaultAsync(o => o.Email == email);
            return Owner;
        }
        public async Task<IEnumerable<Owner>> GetOwners()
        {
            return await _context.Owners
                .Include(o => o.ChargingStations)
                .ThenInclude(c => c.Address)
                .ToListAsync();
        }

        public async Task<Owner> UpdateOwner(Owner Owner)
        {
            // find Owner by id
            var foundOwner = await _context.Owners.FindAsync(Owner.Id);
            // check if Owner is null
            if (foundOwner == null)
                return null;
            // update Owner
            foundOwner.Name = Owner.Name;
            foundOwner.Email = Owner.Email;
            foundOwner.Password = Owner.Password;
            foundOwner.Phone = Owner.Phone;
            foundOwner.Description = Owner.Description;

            // save changes and return updated Owner
            await _context.SaveChangesAsync();
            return foundOwner;
        }
    }
}
