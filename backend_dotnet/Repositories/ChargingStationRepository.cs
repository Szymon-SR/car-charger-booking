﻿using backend_dotnet.Data;
using backend_dotnet.Models;
using Microsoft.EntityFrameworkCore;

namespace backend_dotnet.Repositories
{
    public class ChargingStationRepository : IChargingStationRepository
    {

        private readonly ChargingStationContext _context;
        public ChargingStationRepository(ChargingStationContext context)
        {
            _context = context;
        }

        public async Task<ChargingStation> AddChargingStation(ChargingStation ChargingStation)
        {
            var result = await _context.ChargingStations.AddAsync(ChargingStation);
            await _context.SaveChangesAsync();
            return result.Entity;
        }


        public async Task<ChargingStation> DeleteChargingStation(int id)
        {
            var ChargingStation = await _context.ChargingStations.FindAsync(id);
            if (ChargingStation == null)
                return null;


            _context.ChargingStations.Remove(ChargingStation);
            await _context.SaveChangesAsync();
            return ChargingStation;
        }

        public async Task<ChargingStation> GetChargingStation(int id)
        {
            var ChargingStation = await _context.ChargingStations
                .Include(cs => cs.Address).Include(cs => cs.Owner).FirstOrDefaultAsync(cs => cs.Id == id);
            return ChargingStation;
        }

        public async Task<IEnumerable<ChargingStation>> GetChargingStations()
        {
            return await _context.ChargingStations.Include(cs => cs.Address).Include(cs => cs.Owner).ToListAsync();
        }

        public async Task<ChargingStation> UpdateChargingStation(ChargingStation ChargingStation)
        {
            // find ChargingStation by id
            var foundChargingStation = await _context.ChargingStations.FindAsync(ChargingStation.Id);
            // check if ChargingStation is null
            if (foundChargingStation == null)
                return null;
            // update ChargingStation
            foundChargingStation.Name = ChargingStation.Name;
            foundChargingStation.Address = ChargingStation.Address;
            foundChargingStation.Description = ChargingStation.Description;
            foundChargingStation.Owner = ChargingStation.Owner;
            foundChargingStation.Active = ChargingStation.Active;
            foundChargingStation.ChargerType = ChargingStation.ChargerType;

            // save changes and return updated ChargingStation
            await _context.SaveChangesAsync();
            return foundChargingStation;
        }
    }
}
