﻿using backend_dotnet.Data;
using backend_dotnet.Models;
using Microsoft.EntityFrameworkCore;

namespace backend_dotnet.Repositories
{
    public class ReservationRepository : IReservationRepository
    {

        private readonly ChargingStationContext _context;
        public ReservationRepository(ChargingStationContext context)
        {
            _context = context;
        }

        public async Task<Reservation> AddReservation(Reservation Reservation)
        {
            var result = await _context.Reservations.AddAsync(Reservation);
            await _context.SaveChangesAsync();
            return result.Entity;
        }


        public async Task<Reservation> DeleteReservation(int id)
        {
            var Reservation = await _context.Reservations.FindAsync(id);
            if (Reservation == null)
                return null;


            _context.Reservations.Remove(Reservation);
            await _context.SaveChangesAsync();
            return Reservation;
        }

        public async Task<Reservation> GetReservation(int id)
        {
            var Reservation = await _context.Reservations
                .Include(r => r.ChargingStation)
                .Include(r => r.ChargingStation.Address)
                .Include(r => r.Client)
                .FirstOrDefaultAsync(r => r.Id == id);
            return Reservation;
        }

        public async Task<IEnumerable<Reservation>> GetReservations()
        {
            return await _context.Reservations
                .Include(r => r.ChargingStation)
                .Include(r => r.ChargingStation.Address)
                .Include(r => r.Client)
                .ToListAsync();
        }

        public async Task<Reservation> UpdateReservation(Reservation Reservation)
        {
            // find Reservation by id
            var foundReservation = await _context.Reservations.FindAsync(Reservation.Id);
            // check if Reservation is null
            if (foundReservation == null)
                return null;
            // update Reservation
            foundReservation.Client = Reservation.Client;
            foundReservation.ChargingStation = Reservation.ChargingStation;
            foundReservation.StartDate = Reservation.StartDate;
            foundReservation.EndDate = Reservation.EndDate;
            foundReservation.Confirmed = Reservation.Confirmed;
            foundReservation.Active = Reservation.Active;

            // save changes and return updated Reservation
            await _context.SaveChangesAsync();
            return foundReservation;
        }
    }
}
