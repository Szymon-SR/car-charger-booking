using backend_dotnet.Models;

namespace backend_dotnet.Repositories
{
    public interface IPostRepository
    {
        Task<IEnumerable<Post>> GetPosts();
        Task<Post> GetPost(int id);
        Task<Post> AddPost(Post Post);
        Task<Post> UpdatePost(Post Post);
        Task<Post> DeletePost(int id);
    }
}