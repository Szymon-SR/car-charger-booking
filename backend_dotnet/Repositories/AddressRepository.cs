﻿using backend_dotnet.Data;
using backend_dotnet.Models;
using Microsoft.EntityFrameworkCore;

namespace backend_dotnet.Repositories
{
    public class AddressRepository : IAddressRepository
    {

        private readonly ChargingStationContext _context;
        public AddressRepository(ChargingStationContext context)
        {
            _context = context;
        }

        public async Task<Address> AddAddress(Address Address)
        {
            var result = await _context.Addresses.AddAsync(Address);
            await _context.SaveChangesAsync();
            return result.Entity;
        }


        public async Task<Address> DeleteAddress(int id)
        {
            var address = await _context.Addresses.FindAsync(id);
            if (address == null)
                return null;


            _context.Addresses.Remove(address);
            await _context.SaveChangesAsync();
            return address;
        }

        public async Task<Address> GetAddress(int id)
        {
            var address = await _context.Addresses.FindAsync(id);
            return address;
        }

        public async Task<IEnumerable<Address>> GetAddresss()
        {
            return await _context.Addresses.ToListAsync();
        }

        public async Task<Address> UpdateAddress(Address Address)
        {
            // find Address by id
            var foundAddress = await _context.Addresses.FindAsync(Address.Id);
            // check if Address is null
            if (foundAddress == null)
                return null;
            // update Address
            foundAddress.Street = Address.Street;
            foundAddress.PostalCode = Address.PostalCode;
            foundAddress.City = Address.City;
            foundAddress.Country = Address.Country;
            foundAddress.Latitude = Address.Latitude;
            foundAddress.Longitude = Address.Longitude;

            // save changes and return updated Address
            await _context.SaveChangesAsync();
            return foundAddress;
        }
    }
}
