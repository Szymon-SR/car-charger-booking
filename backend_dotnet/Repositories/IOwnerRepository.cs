using backend_dotnet.Models;

namespace backend_dotnet.Repositories
{
    public interface IOwnerRepository
    {
        Task<IEnumerable<Owner>> GetOwners();
        Task<Owner> GetOwnerByEmail(string email);
        Task<Owner> GetOwner(int id);
        Task<Owner> AddOwner(Owner Owner);
        Task<Owner> UpdateOwner(Owner Owner);
        Task<Owner> DeleteOwner(int id);
    }
}