﻿using backend_dotnet.Data;
using backend_dotnet.Models;
using Microsoft.EntityFrameworkCore;

namespace backend_dotnet.Repositories
{
    public class ClientRepository : IClientRepository
    {

        private readonly ChargingStationContext _context;
        public ClientRepository(ChargingStationContext context)
        {
            _context = context;
        }

        public async Task<Client> AddClient(Client Client)
        {
            var result = await _context.Clients.AddAsync(Client);
            await _context.SaveChangesAsync();
            return result.Entity;
        }


        public async Task<Client> DeleteClient(int id)
        {
            var Client = await _context.Clients.FindAsync(id);
            if (Client == null)
                return null;


            _context.Clients.Remove(Client);
            await _context.SaveChangesAsync();
            return Client;
        }

        // get client by email
        public async Task<Client> GetClientByEmail(string email)
        {
            var Client = await _context.Clients.FirstOrDefaultAsync(c => c.Email == email);
            return Client;
        }

        public async Task<Client> GetClient(int id)
        {
            var Client = await _context.Clients.FindAsync(id);
            return Client;
        }

        public async Task<IEnumerable<Client>> GetClients()
        {
            return await _context.Clients.ToListAsync();
        }

        public async Task<Client> UpdateClient(Client Client)
        {
            // find Client by id
            var foundClient = await _context.Clients.FindAsync(Client.Id);
            // check if Client is null
            if (foundClient == null)
                return null;
            // update Client
            foundClient.Email = Client.Email;
            foundClient.FirstName = Client.FirstName;
            foundClient.LastName = Client.LastName;
            foundClient.Password = Client.Password;
            foundClient.Phone = Client.Phone;
            foundClient.ChargerType = Client.ChargerType;

            // save changes and return updated Client
            await _context.SaveChangesAsync();
            return foundClient;
        }
    }
}
