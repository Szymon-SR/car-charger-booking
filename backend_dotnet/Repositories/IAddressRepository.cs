using backend_dotnet.Models;

namespace backend_dotnet.Repositories
{
    public interface IAddressRepository
    {
        Task<IEnumerable<Address>> GetAddresss();
        Task<Address> GetAddress(int id);
        Task<Address> AddAddress(Address Address);
        Task<Address> UpdateAddress(Address Address);
        Task<Address> DeleteAddress(int id);
    }
}