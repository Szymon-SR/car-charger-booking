﻿using backend_dotnet.Data;
using backend_dotnet.Models;
using Microsoft.EntityFrameworkCore;

namespace backend_dotnet.Repositories
{
    public class PostRepository : IPostRepository
    {

        private readonly ChargingStationContext _context;
        public PostRepository(ChargingStationContext context)
        {
            _context = context;
        }

        public async Task<Post> AddPost(Post Post)
        {
            var result = await _context.Posts.AddAsync(Post);
            await _context.SaveChangesAsync();
            return result.Entity;
        }


        public async Task<Post> DeletePost(int id)
        {
            var Post = await _context.Posts.FindAsync(id);
            if (Post == null)
                return null;


            _context.Posts.Remove(Post);
            await _context.SaveChangesAsync();
            return Post;
        }

        public async Task<Post> GetPost(int id)
        {
            var Post = await _context.Posts
                .Include(p => p.Client)
                .Include(p => p.ChargingStation)
                .FirstOrDefaultAsync(p => p.Id == id);
            return Post;
        }

        public async Task<IEnumerable<Post>> GetPosts()
        {
            return await _context.Posts
                .Include(p => p.Client)
                .Include(p => p.ChargingStation)
                .ToListAsync();
        }

        public async Task<Post> UpdatePost(Post Post)
        {
            // find Post by id
            var foundPost = await _context.Posts.FindAsync(Post.Id);
            // check if Post is null
            if (foundPost == null)
                return null;
            // update Post
            foundPost.ChargingStation = Post.ChargingStation;
            foundPost.Client = Post.Client;
            foundPost.Content = Post.Content;
            foundPost.StartDate = Post.StartDate;
            foundPost.Stars = Post.Stars;
            foundPost.Car = Post.Car;

            // save changes and return updated Post
            await _context.SaveChangesAsync();
            return foundPost;
        }
    }
}
