using backend_dotnet.Models;

namespace backend_dotnet.Repositories
{
    public interface IReservationRepository
    {
        Task<IEnumerable<Reservation>> GetReservations();
        Task<Reservation> GetReservation(int id);
        Task<Reservation> AddReservation(Reservation Reservation);
        Task<Reservation> UpdateReservation(Reservation Reservation);
        Task<Reservation> DeleteReservation(int id);
    }
}