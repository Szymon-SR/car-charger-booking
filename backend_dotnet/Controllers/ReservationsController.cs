﻿using backend_dotnet.Models;
using backend_dotnet.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace backend_dotnet.Controllers
{
    [Route("reservations")]
    [ApiController]
    public class ReservationsController : ControllerBase
    {
        private readonly IReservationRepository _reservationRepository;
        private readonly IChargingStationRepository _chargingStationRepository;
        private readonly IClientRepository _clientRepository;

        public ReservationsController(IReservationRepository reservationRepository, IChargingStationRepository chargingStationRepository, IClientRepository clientRepository)
        {
            _reservationRepository = reservationRepository;
            _chargingStationRepository = chargingStationRepository;
            _clientRepository = clientRepository;
        }

        // GET: api/Reservations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Reservation>>> GetReservations()
        {
            if (_reservationRepository.GetReservations == null)
            {
                return NotFound();
            }
            return Ok(await _reservationRepository.GetReservations());
        }

        // GET: api/Reservations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Reservation>> GetReservation(int id)
        {
            if (_reservationRepository.GetReservations == null)
            {
                return NotFound();
            }
            var reservation = await _reservationRepository.GetReservation(id);

            if (reservation == null)
            {
                return NotFound();
            }

            return reservation;
        }

        // PUT: api/Reservations/5
        // To protect from overreservationing attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutReservation(int id, ReservationDTO reservationDto)
        {
            if (id != reservationDto.Id)
            {
                return BadRequest();
            }

            var reservationToUpdate = await _reservationRepository.GetReservation(id);
            if (reservationToUpdate == null)
                return NotFound();

            var foundChargingStation = await _chargingStationRepository.GetChargingStation(reservationDto.ChargingStationId);

            if (foundChargingStation == null)
                return NotFound();

            var foundClient = await _clientRepository.GetClient(reservationDto.ClientId);

            if (foundClient == null)
                return NotFound();



            reservationToUpdate.ChargingStation = foundChargingStation;
            reservationToUpdate.Client = foundClient;
            reservationToUpdate.StartDate = reservationDto.StartDate;
            reservationToUpdate.EndDate = reservationDto.EndDate;
            reservationToUpdate.Confirmed = reservationDto.Confirmed;
            reservationToUpdate.Active = reservationDto.Active;

            var updatedReservations = await _reservationRepository.UpdateReservation(reservationToUpdate);

            if (updatedReservations == null)
            {
                return NotFound();
            }
            return Ok(updatedReservations);
        }

        // POST: api/Reservations
        // To protect from overreservationing attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Reservation>> ReservationReservation(ReservationDTO reservationDto)
        {
            if (reservationDto == null)
                return BadRequest();

            var reservationChargingStation = await _chargingStationRepository.GetChargingStation(reservationDto.ChargingStationId);
            if (reservationChargingStation == null)
                return NotFound();

            var reservationClient = await _clientRepository.GetClient(reservationDto.ClientId);
            if (reservationClient == null)
                return NotFound();

            var reservationToAdd = new Reservation
            {
                ChargingStation = reservationChargingStation,
                Client = reservationClient,
                StartDate = reservationDto.StartDate,
                EndDate = reservationDto.EndDate,
                Confirmed = reservationDto.Confirmed,
                Active = reservationDto.Active,
            };

            var addedReservation = await _reservationRepository.AddReservation(reservationToAdd);

            return CreatedAtAction(nameof(GetReservations), new { id = addedReservation.Id }, addedReservation);
        }

        // DELETE: api/Reservations/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteReservation(int id)
        {
            var reservationToDelete = await _reservationRepository.GetReservation(id);

            if (reservationToDelete == null)
            {
                return NotFound($"Reservation with Id = {id} not found");
            }
            var deletedReservations = await _reservationRepository.DeleteReservation(id);

            return Ok(deletedReservations);
        }
    }
}
