﻿using backend_dotnet.Models;
using backend_dotnet.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace backend_dotnet.Controllers
{
    [Route("owners")]
    [ApiController]
    public class OwnersController : ControllerBase
    {
        private readonly IOwnerRepository _ownerRepository;
        private readonly IChargingStationRepository _chargingStationRepository;

        public OwnersController(IOwnerRepository ownerRepository, IChargingStationRepository chargingStationRepository)
        {
            _ownerRepository = ownerRepository;
            _chargingStationRepository = chargingStationRepository;
        }

        // GET: api/Owners
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Owner>>> GetOwners()
        {
            if (_ownerRepository.GetOwners == null)
            {
                return NotFound();
            }
            return Ok(await _ownerRepository.GetOwners());
        }

        // GET: api/Owners/5
        [HttpGet("{email}")]
        public async Task<ActionResult<Owner>> GetOwner(string email)
        {
            if (_ownerRepository.GetOwners == null)
            {
                return NotFound();
            }
            var owner = await _ownerRepository.GetOwnerByEmail(email);

            if (owner == null)
            {
                return NotFound();
            }

            return owner;
        }

        // PUT: api/Owners/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOwner(int id, OwnerDTO ownerDto)
        {
            if (id != ownerDto.Id)
            {
                return BadRequest();
            }

            var ownerToUpdate = await _ownerRepository.GetOwner(id);
            if (ownerToUpdate == null)
            {
                return NotFound();
            }

            var chargingStations = await _chargingStationRepository.GetChargingStations();
            var ownerChargingStations = chargingStations.Where(c => c.Owner.Id == ownerDto.Id).ToList();

            ownerToUpdate.Name = ownerDto.Name;
            ownerToUpdate.Email = ownerDto.Email;
            ownerToUpdate.Password = ownerDto.Password;
            ownerToUpdate.Phone = ownerDto.Phone;
            ownerToUpdate.Description = ownerDto.Description;
            ownerToUpdate.ChargingStations = ownerChargingStations;

            var updatedOwner = await _ownerRepository.UpdateOwner(ownerToUpdate);

            return Ok(updatedOwner);
        }

        // POST: api/Owners
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Owner>> PostOwner(OwnerDTO ownerDto)
        {
            if (ownerDto == null)
                return BadRequest();

            var chargingStations = await _chargingStationRepository.GetChargingStations();
            var ownerChargingStations = chargingStations.Where(c => c.Owner.Id == ownerDto.Id).ToList();

            var ownerToAdd = new Owner
            {
                Name = ownerDto.Name,
                Email = ownerDto.Email,
                Password = ownerDto.Password,
                Phone = ownerDto.Phone,
                Description = ownerDto.Description,
                ChargingStations = ownerChargingStations
            };

            var addedOwner = await _ownerRepository.AddOwner(ownerToAdd);

            return CreatedAtAction(nameof(GetOwners), new { id = addedOwner.Id }, addedOwner);
        }

        [HttpPost("login")]
        public async Task<ActionResult<Client>> Login(OwnerDTO ownerDTO)
        {
            if (ownerDTO == null)
                return BadRequest();

            var loggedOwner = await _ownerRepository.GetOwnerByEmail(ownerDTO.Email);

            if (loggedOwner == null)
            {
                return NotFound("Owner with email not found");
            }

            if (loggedOwner.Password != ownerDTO.Password)
            {
                return BadRequest("Wrong password");
            }

            return Ok(loggedOwner);
        }

        // DELETE: api/Owners/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOwner(int id)
        {
            var ownerToDelete = await _ownerRepository.GetOwner(id);

            if (ownerToDelete == null)
            {
                return NotFound($"Owner with Id = {id} not found");
            }
            var deletedOwners = await _ownerRepository.DeleteOwner(id);

            return Ok(deletedOwners);
        }
    }
}
