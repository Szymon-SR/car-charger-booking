﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_dotnet.Data;
using backend_dotnet.Models;
using backend_dotnet.Repositories;
using System.Net;

namespace backend_dotnet.Controllers
{
    [Route("stations")]
    [ApiController]
    public class ChargingStationsController : ControllerBase
    {
        private readonly IChargingStationRepository _chargingStationRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly IOwnerRepository _ownerRepository;

        public ChargingStationsController(IChargingStationRepository chargingStationRepository, IAddressRepository addressRepository, IOwnerRepository ownerRepository)
        {
            _chargingStationRepository = chargingStationRepository;
            _addressRepository = addressRepository;
            _ownerRepository = ownerRepository;
        }

        // GET: api/ChargingStations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ChargingStation>>> GetChargingStations()
        {
            if (_chargingStationRepository.GetChargingStations == null)
            {
                return NotFound();
            }
            return Ok(await _chargingStationRepository.GetChargingStations());
        }

        // GET: api/ChargingStations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ChargingStation>> GetChargingStation(int id)
        {
            if (_chargingStationRepository.GetChargingStations == null)
            {
                return NotFound();
            }
            var chargingStation = await _chargingStationRepository.GetChargingStation(id);

            if (chargingStation == null)
            {
                return NotFound();
            }

            return chargingStation;
        }

        // PUT: api/ChargingStations/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutChargingStation(int id, ChargingStationDTO chargingStationDto)
        {
            if (id != chargingStationDto.Id)
            {
                return BadRequest();
            }

            var chargingStationToUpdate = await _chargingStationRepository.GetChargingStation(id);
            if (chargingStationToUpdate == null)
            {
                return NotFound();
            }

            chargingStationToUpdate.Name = chargingStationDto.Name;
            chargingStationToUpdate.Address.Id = chargingStationDto.AddressId;
            chargingStationToUpdate.Owner.Id = chargingStationDto.OwnerId;
            chargingStationToUpdate.Description = chargingStationDto.Description;
            chargingStationToUpdate.Active = chargingStationDto.Active;
            chargingStationToUpdate.ChargerType = chargingStationDto.ChargerType;

            var updatedChargingStation = await _chargingStationRepository.UpdateChargingStation(chargingStationToUpdate);

            return Ok(updatedChargingStation);
        }

        // POST: api/ChargingStations
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ChargingStation>> PostChargingStation(ChargingStationDTO chargingStationDto)
        {
            if (chargingStationDto == null)
                return BadRequest();

            var chargingStationAddress = await _addressRepository.GetAddress(chargingStationDto.AddressId);

            if (chargingStationAddress == null)
                return NotFound();

            var chargingStationOwner = await _ownerRepository.GetOwner(chargingStationDto.OwnerId);

            if (chargingStationOwner == null)
                return NotFound();

            var chargingStationToAdd = new ChargingStation
            {
                Name = chargingStationDto.Name,
                Address = chargingStationAddress,
                Owner = chargingStationOwner,
                Description = chargingStationDto.Description,
                Active = chargingStationDto.Active,
                ChargerType = chargingStationDto.ChargerType
            };

            var addedChargingStation = await _chargingStationRepository.AddChargingStation(chargingStationToAdd);

            return CreatedAtAction(nameof(GetChargingStation), new { id = addedChargingStation.Id }, addedChargingStation);
        }

        // DELETE: api/ChargingStations/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteChargingStation(int id)
        {
            var chargingStationToDelete = await _chargingStationRepository.GetChargingStation(id);

            if (chargingStationToDelete == null)
            {
                return NotFound($"ChargingStation with Id = {id} not found");
            }
            var deletedChargingStation = await _chargingStationRepository.DeleteChargingStation(id);

            return Ok(deletedChargingStation);
        }
    }
}
