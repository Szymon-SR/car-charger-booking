﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_dotnet.Data;
using backend_dotnet.Models;
using backend_dotnet.Repositories;

namespace backend_dotnet.Controllers
{
    [Route("clients")]
    [ApiController]
    public class ClientsController : ControllerBase
    {
        private readonly IClientRepository _clientRepository;

        public ClientsController(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }

        // GET: api/Clients
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Client>>> GetClients()
        {
            if (_clientRepository.GetClients == null)
            {
                return NotFound();
            }
            return Ok(await _clientRepository.GetClients());
        }


        //get client by email
        [HttpGet("email/{email}")]
        public async Task<ActionResult<Client>> GetClientByEmail(string email)
        {
            if (_clientRepository.GetClients == null)
            {
                return NotFound();
            }
            var client = await _clientRepository.GetClientByEmail(email);

            if (client == null)
            {
                return NotFound();
            }

            return client;
        }


        // GET: api/Clients/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Client>> GetClient(int id)
        {
            if (_clientRepository.GetClients == null)
            {
                return NotFound();
            }
            var client = await _clientRepository.GetClient(id);

            if (client == null)
            {
                return NotFound();
            }

            return client;
        }

        // PUT: api/Clients/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutClient(int id, ClientDTO clientDto)
        {
            if (id != clientDto.Id)
            {
                return BadRequest();
            }

            var clientToUpdate = await _clientRepository.GetClient(id);
            if (clientToUpdate == null)
            {
                return NotFound();
            }

            clientToUpdate.Email = clientDto.Email;
            clientToUpdate.Password = clientDto.Password;
            clientToUpdate.FirstName = clientDto.FirstName;
            clientToUpdate.LastName = clientDto.LastName;
            clientToUpdate.Phone = clientDto.Phone;
            clientToUpdate.ChargerType = clientDto.ChargerType;

            var updatedClient = await _clientRepository.UpdateClient(clientToUpdate);

            return Ok(updatedClient);
        }

        // POST: api/Clients
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Client>> PostClient(ClientDTO clientDTO)
        {
            if (clientDTO == null)
                return BadRequest();

            var client = new Client
            {
                Email = clientDTO.Email,
                Password = clientDTO.Password,
                FirstName = clientDTO.FirstName,
                LastName = clientDTO.LastName,
                Phone = clientDTO.Phone,
                ChargerType = clientDTO.ChargerType
            };

            var addedClient = await _clientRepository.AddClient(client);

            return CreatedAtAction(nameof(GetClients),
                new { id = addedClient.Id }, addedClient);
        }

        // post login
        [HttpPost("login")]
        public async Task<ActionResult<Client>> Login(ClientDTO clientDTO)
        {
            if (clientDTO == null)
                return BadRequest();

            var loggedClient = await _clientRepository.GetClientByEmail(clientDTO.Email);

            if (loggedClient == null)
            {
                return NotFound("Client with email not found");
            }

            if (loggedClient.Password != clientDTO.Password)
            {
                return BadRequest("Wrong password");
            }

            return Ok(loggedClient);
        }

        // post register
        [HttpPost("register")]
        public async Task<ActionResult<Client>> Register(ClientDTO clientDTO)
        {
            if (clientDTO == null)
                return BadRequest();

            var foundClient = await _clientRepository.GetClientByEmail(clientDTO.Email);
            if (foundClient != null)
            {
                return BadRequest("Client with email already exists");
            }

            var client = new Client
            {
                Email = clientDTO.Email,
                Password = clientDTO.Password,
                FirstName = clientDTO.FirstName,
                LastName = clientDTO.LastName,
                Phone = clientDTO.Phone,
                ChargerType = clientDTO.ChargerType
            };

            var addedClient = await _clientRepository.AddClient(client);

            return CreatedAtAction(nameof(GetClients),
                new { id = addedClient.Id }, addedClient);
        }

        // DELETE: api/Clients/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClient(int id)
        {
            var clientToDelete = await _clientRepository.GetClient(id);

            if (clientToDelete == null)
            {
                return NotFound($"Client with Id = {id} not found");
            }
            var deletedClients = await _clientRepository.DeleteClient(id);

            return Ok(deletedClients);
        }

    }
}
