﻿using backend_dotnet.Models;
using backend_dotnet.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace backend_dotnet.Controllers
{
    [Route("adresses")]
    [ApiController]
    public class AddressesController : ControllerBase
    {
        private readonly IAddressRepository _addressRepository;

        public AddressesController(IAddressRepository addressRepository)
        {
            _addressRepository = addressRepository;
        }

        // GET: api/Addresses
        [HttpGet]
        public async Task<ActionResult> GetAddresses()
        {
            if (_addressRepository.GetAddresss == null)
            {
                return NotFound();
            }
            return Ok(await _addressRepository.GetAddresss());
        }

        // GET: api/Addresses/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Address>> GetAddress(int id)
        {
            if (_addressRepository.GetAddresss == null)
            {
                return NotFound();
            }
            var address = await _addressRepository.GetAddress(id);

            if (address == null)
            {
                return NotFound();
            }

            return address;
        }

        // PUT: api/Addresses/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAddress(int id, AddressDTO addressDto)
        {
            if (id != addressDto.Id)
            {
                return BadRequest();
            }

            var addressToUpdate = await _addressRepository.GetAddress(id);
            if (addressToUpdate == null)
            {
                return NotFound();
            }

            addressToUpdate.Street = addressDto.Street;
            addressToUpdate.City = addressDto.City;
            addressToUpdate.PostalCode = addressDto.PostalCode;
            addressToUpdate.Country = addressDto.Country;
            addressToUpdate.Latitude = addressDto.Latitude;
            addressToUpdate.Longitude = addressDto.Longitude;

            var updatedAddress = await _addressRepository.UpdateAddress(addressToUpdate);

            return Ok(updatedAddress);
        }

        // POST: api/Addresses
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Address>> PostAddress([FromBody] AddressDTO addressDto)
        {
            if (addressDto == null)
                return BadRequest();

            var address = new Address
            {
                Street = addressDto.Street,
                City = addressDto.City,
                PostalCode = addressDto.PostalCode,
                Country = addressDto.Country,
                Latitude = addressDto.Latitude,
                Longitude = addressDto.Longitude
            };

            var addedAddress = await _addressRepository.AddAddress(address);

            return CreatedAtAction(nameof(GetAddress), new { id = addedAddress.Id }, addedAddress);
        }

        // DELETE: api/Addresses/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAddress(int id)
        {
            var addressToDelete = await _addressRepository.GetAddress(id);

            if (addressToDelete == null)
            {
                return NotFound($"Address with Id = {id} not found");
            }
            var deletedAddress = await _addressRepository.DeleteAddress(id);

            return Ok(deletedAddress);
        }
    }
}
