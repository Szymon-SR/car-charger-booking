﻿using Azure.Core;
using backend_dotnet.Data;
using backend_dotnet.Models;
using backend_dotnet.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Policy;

namespace backend_dotnet.Controllers
{
    [Route("gps")]
    [ApiController]
    public class GpsController : ControllerBase
    {
        private readonly ChargingStationContext _context;

        public GpsController(ChargingStationContext context)
        {
            _context = context;
        }

        [HttpGet]
        // method to take in a lat and long and return the closest charging station

        public async Task<ActionResult<IEnumerable<Tuple<ChargingStation, double>>>> GetChargingStationsSortedByDistance([FromQuery] double latitude, [FromQuery] double longitude)
        {
            if (_context.ChargingStations == null)
            {
                return NotFound();
            }
            // get all charging stations and sort them by distance from the given lat and long using the GetDistance method and Address.Latitude and Address.Longitude in ChargingStation
            var chargingStations = await _context.ChargingStations.Include(cs => cs.Address).ToListAsync();
            // map all cahrgingStations and add a new Tuple<ChargingStation, double> property DistanceFromUser that is the distance from the given lat and long

            var chargingStationsWithDistance = chargingStations.Select(cs => 
                new { 
                    cs, 
                    DistanceFromUser = GetDistance(latitude, longitude, cs.Address.Latitude, cs.Address.Longitude) 
                }
            ).ToList();


            // sort the chargingStations by the DistanceFromUser property
            chargingStationsWithDistance.Sort((cs1, cs2) => cs1.DistanceFromUser.CompareTo(cs2.DistanceFromUser));

            return Ok(chargingStationsWithDistance);
        }

        // method to take in a ChargingStationId and return the distance between it and the given lat and long
        [HttpGet("{id}")]
        public async Task<ActionResult<double>> GetDistanceFromChargingStation([FromRoute] int id, [FromQuery] double latitude, [FromQuery] double longitude)
        {
            if (_context.ChargingStations == null)
            {
                return NotFound();
            }
            var chargingStation = await _context.ChargingStations.FindAsync(id);
            if (chargingStation == null)
            {
                return NotFound();
            }
            return Ok(GetDistance(latitude, longitude, chargingStation.Address.Latitude, chargingStation.Address.Longitude));
        }

        // method that takes in a lat and long of one point and lat and long of another point and returns the distance between them using heversine method
        // https://en.wikipedia.org/wiki/Haversine_formula
        private double GetDistance(double latitude1, double longitude1, double latitude2, double longitude2)
        {
            double R = 6371; // Earth's radius in km

            double lat1 = Math.PI * latitude1 / 180.0;
            double lat2 = Math.PI * latitude2 / 180.0;
            double lon1 = Math.PI * longitude1 / 180.0;
            double lon2 = Math.PI * longitude2 / 180.0;

            double dLat = lat2 - lat1;
            double dLon = lon2 - lon1;

            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                       Math.Cos(lat1) * Math.Cos(lat2) *
                       Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            double distance = R * c;

            return Math.Round(distance, 2, MidpointRounding.ToZero);
        }
    }
}
