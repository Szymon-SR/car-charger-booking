﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_dotnet.Data;
using backend_dotnet.Models;
using backend_dotnet.Repositories;

namespace backend_dotnet.Controllers
{
    [Route("posts")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly IPostRepository _postRepository;
        private readonly IChargingStationRepository _chargingStationRepository;
        private readonly IClientRepository _clientRepository;

        public PostsController(IPostRepository postRepository, IChargingStationRepository chargingStationRepository, IClientRepository clientRepository)
        {
            _postRepository = postRepository;
            _chargingStationRepository = chargingStationRepository;
            _clientRepository = clientRepository;
        }

        // GET: api/Posts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Post>>> GetPosts()
        {
            if (_postRepository.GetPosts == null)
            {
                return NotFound();
            }
            return Ok(await _postRepository.GetPosts());
        }

        // GET: api/Posts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Post>> GetPost(int id)
        {
            if (_postRepository.GetPosts == null)
            {
                return NotFound();
            }
            var post = await _postRepository.GetPost(id);

            if (post == null)
            {
                return NotFound();
            }

            return post;
        }

        // PUT: api/Posts/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPost(int id, PostDTO postDto)
        {
            if (id != postDto.Id)
            {
                return BadRequest();
            }

            var postToUpdate = await _postRepository.GetPost(id);
            if (postToUpdate == null)
            {
                return NotFound();
            }

            postToUpdate.ChargingStation.Id = postDto.ChargingStationId;
            postToUpdate.Client.Id = postDto.ClientId;
            postToUpdate.Content = postDto.Content;
            postToUpdate.StartDate = postDto.StartDate;
            postToUpdate.Stars = postDto.Stars;
            postToUpdate.Car = postDto.Car;

            var updatedPosts = await _postRepository.UpdatePost(postToUpdate);
            if (updatedPosts == null)
            {
                return NotFound();
            }

            return Ok(updatedPosts);
        }

        // POST: api/Posts
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Post>> PostPost(PostDTO postDto)
        {
            if (postDto == null)
                return BadRequest();

            var postChargingStation = await _chargingStationRepository.GetChargingStation(postDto.ChargingStationId);
            if (postChargingStation == null)
                return NotFound();

            var postClient = await _clientRepository.GetClient(postDto.ClientId);
            if (postClient == null)
                return NotFound();

            var postToAdd = new Post
            {
                ChargingStation = postChargingStation,
                Client = postClient,
                Content = postDto.Content,
                StartDate = postDto.StartDate,
                Stars = postDto.Stars,
                Car = postDto.Car,
            };

            var addedPost = await _postRepository.AddPost(postToAdd);

            return CreatedAtAction(nameof(GetPosts), new { id = addedPost.Id }, addedPost);
        }

        // DELETE: api/Posts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePost(int id)
        {
            var postToDelete = await _postRepository.GetPost(id);

            if (postToDelete == null)
            {
                return NotFound($"Post with Id = {id} not found");
            }
            var deletedPosts = await _postRepository.DeletePost(id);

            return Ok(deletedPosts);
        }
    }
}
