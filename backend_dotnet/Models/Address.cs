﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace backend_dotnet.Models
{
    /*
    CREATE TABLE Address (
     AddressId int PRIMARY KEY,
     Street varchar(100) NOT NULL,
     PostalCode varchar(20) NOT NULL,
     City varchar(100) NOT NULL,
     Country varchar(100) NOT NULL,
     Latitude float NOT NULL,
     Longitude float NOT NULL
    );
     */
    public class Address
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Street { get; set; }
        [Required]
        [MaxLength(20)]
        public string PostalCode { get; set; }
        [Required]
        [MaxLength(100)]
        public string City { get; set; }
        [Required]
        [MaxLength(100)]
        public string Country { get; set; }
        [Required]
        public float Latitude { get; set; }
        [Required]
        public float Longitude { get; set; }
    }
}
