﻿using System.Data;
using System.Security.Cryptography.Xml;
using System.Xml.Linq;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;

namespace backend_dotnet.Models
{

 //CREATE TABLE ChargingStation (
/*    ChargingStationId int PRIMARY KEY,
 OwnerId int NOT NULL,
 Name varchar(50) NOT NULL,
 AddressId int NOT NULL,
 Description varchar(100),
 Active bit NOT NULL,
    CONSTRAINT FK_ChargingStation_Owner FOREIGN KEY(OwnerId) REFERENCES
Owner(OwnerId),
 CONSTRAINT FK_ChargingStation_Address FOREIGN KEY(AddressId) REFERENCES
Address(AddressId)
);*/
    public class ChargingStation
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public Owner Owner{ get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        public Address Address { get; set; }
        [MaxLength(100)]
        public string Description { get; set; }
        public bool Active { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public AdapterPlug ChargerType { get; set; } = AdapterPlug.CCS;
    }
}
