﻿
using Newtonsoft.Json;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace backend_dotnet.Models
{
    public class Owner
    {
        // set generation strategy to sequence
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        [MaxLength(50)]
        public string Email { get; set; }

        [Required]
        [MaxLength(50)]
        public string Password { get; set; }

        [MaxLength(20)]
        public string Phone { get; set; }
        [MaxLength(255)]
        public string Description { get; set; }
        // not include in json response by default (use [JsonIgnore] to exclude from json response)
        public List<ChargingStation>? ChargingStations { get; set; }
    }
}
