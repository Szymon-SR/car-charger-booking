﻿namespace backend_dotnet.Models
{
    public class OwnerDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string Description { get; set; }
        public List<int>? ChargingStationsIds { get; set; }
    }
}
