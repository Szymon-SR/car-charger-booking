﻿namespace backend_dotnet.Models
{
    public class PostDTO
    {
        public int Id { get; set; }
        public string Content { get; set; } = default!;
        public int ChargingStationId { get; set; }
        public int ClientId { get; set; }
        public DateTime StartDate { get; set; }
        public int Stars { get; set; }
        public string? Car { get; set; }
    }
}
