﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace backend_dotnet.Models
{
    /*
     CREATE TABLE Client (
     ClientId int PRIMARY KEY,
     Email varchar(50) NOT NULL,
     Password varchar(50) NOT NULL,
     FirstName varchar(50) NOT NULL,
     LastName varchar(50) NOT NULL,
     Phone varchar(20)
     );
     */
    public class Client
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Email { get; set; }
        [Required]
        [MaxLength(50)]
        public string Password { get; set;}
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set;}
        [Required]
        [MaxLength(50)]
        public string LastName { get; set;}
        [MaxLength(20)]
        public string Phone { get; set;}

        [JsonConverter(typeof(StringEnumConverter))]
        public AdapterPlug ChargerType { get; set; } = AdapterPlug.CCS;
    }
}
