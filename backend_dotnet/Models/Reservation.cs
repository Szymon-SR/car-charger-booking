﻿namespace backend_dotnet.Models
{

    /** Based on this sql generate properties
     CREATE TABLE Reservation (
     ReservationId int PRIMARY KEY,
     ChargingStationId int NOT NULL,
     ClientId varchar(50) NOT NULL,
     StartDate datetime NOT NULL,
     EndDate datetime NOT NULL,
     Confirmed bit NOT NULL,
     Active bit NOT NULL,
     CONSTRAINT FK_Reservation_ChargingStation FOREIGN KEY (ChargingStationId)
    REFERENCES ChargingStation(ChargingStationId),
     CONSTRAINT FK_Reservation_Client FOREIGN KEY (ClientId) REFERENCES
    Client(ClientId)
); 

     */
    public class Reservation
    {
        public int Id { get; set; }
        public ChargingStation ChargingStation { get; set; }
        public Client Client { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Confirmed { get; set; }
        public bool Active { get; set; }
    }
}
