﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace backend_dotnet.Models
{
    public class ChargingStationDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int OwnerId { get; set; }
        public int AddressId { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public AdapterPlug ChargerType { get; set; }
    }
}
