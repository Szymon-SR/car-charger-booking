﻿namespace backend_dotnet.Models
{
    public enum AdapterPlug
    {
        Type1,
        Type2,
        Type3,
        CCS,
        CHAdeMO
    }
}
