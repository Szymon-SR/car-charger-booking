﻿using System.ComponentModel.DataAnnotations;

namespace backend_dotnet.Models
{
    /* based on this sql create properties
     CREATE TABLE Post (
     PostId int PRIMARY KEY,
     ChargingStationId int NOT NULL,
     ClientId varchar(50) NOT NULL,
     Content varchar(max) NOT NULL,
     StartDate datetime NOT NULL,
     Stars int NOT NULL,
     CONSTRAINT FK_Post_ChargingStation FOREIGN KEY (ChargingStationId) REFERENCES
    ChargingStation(ChargingStationId),
     CONSTRAINT FK_Post_Client FOREIGN KEY (ClientId) REFERENCES Client(ClientId)
    );

     */
    public class Post
    {
        public int Id { get; set; }
        [Required]
        public ChargingStation ChargingStation { get; set; }
        [Required]
        public Client Client { get; set; }
        [Required]
        public string Content { get; set; } = default!;
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public int Stars { get; set; }
        public string? Car { get; set; }
    }
}
