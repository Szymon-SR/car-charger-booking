﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace backend_dotnet.Models
{
    public class ClientDTO
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public AdapterPlug ChargerType { get; set; }
    }
}
