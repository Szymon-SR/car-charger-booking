﻿namespace backend_dotnet.Models
{
    public class ReservationDTO
    {
        public int Id { get; set; }
        public int ChargingStationId { get; set; }
        public int ClientId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Confirmed { get; set; }
        public bool Active { get; set; }
    }
}
