﻿using backend_dotnet.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace backend_dotnet.Data
{
    public class ChargingStationContext : DbContext
    {
        public ChargingStationContext(DbContextOptions<ChargingStationContext> opt) : base(opt)
        {

        }

        public DbSet<ChargingStation> ChargingStations { get; set; }
        public DbSet<Owner> Owners { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // delete all tables in DbContext
            modelBuilder.UseSerialColumns();

            modelBuilder.Entity<ChargingStation>()
                .HasOne(c => c.Owner)
                .WithMany(o => o.ChargingStations);

            modelBuilder.Entity<ChargingStation>()
                .Property(c => c.ChargerType)
                .HasConversion(new EnumToStringConverter<AdapterPlug>());

            modelBuilder.Entity<Client>()
                .Property(c => c.ChargerType)
                .HasConversion(new EnumToStringConverter<AdapterPlug>());

            modelBuilder.Entity<ChargingStation>()
                .HasOne(c => c.Address);

            modelBuilder.Entity<Reservation>()
                .HasOne(r => r.ChargingStation);

            modelBuilder.Entity<Reservation>()
                .HasOne(r => r.Client);

            modelBuilder.Entity<Post>()
                .HasOne(p => p.ChargingStation);

            modelBuilder.Entity<Post>()
                .HasOne(p => p.Client);

        }
    }
}
