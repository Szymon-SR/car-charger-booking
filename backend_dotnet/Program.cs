using backend_dotnet.Data;
using backend_dotnet.Repositories;
using backend_dotnet;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

// format code below
if (builder.Environment.IsProduction())
{
    Console.WriteLine("PRODUCTION");
    var dbHost = Environment.GetEnvironmentVariable("DB_HOST");
    var dbName = Environment.GetEnvironmentVariable("DB_NAME");
    var dbPassword = Environment.GetEnvironmentVariable("DB_SA_PASSWORD");
    var connectionString = $"Data Source={dbHost};Initial Catalog={dbName};User ID=sa;Password={dbPassword};Encrypt=False;TrustServerCertificate=False";
    Console.WriteLine(connectionString);
    builder.Services.AddDbContextPool<ChargingStationContext>(options => options.UseSqlServer(connectionString));
}

if (builder.Environment.IsDevelopment())
{
    Console.WriteLine("DEVELOP");
    builder.Services.AddDbContextPool<ChargingStationContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("ChargingStationContext")));
}

builder.Services.Configure<ForwardedHeadersOptions>(options =>
{
    options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
});

builder.Services.AddControllers()
    .AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
    });

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(
        builder =>
        {
            builder.AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod();
        });
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    // options.DocumentFilter<SwaggerBasePathDocumentFilter>("api");
});

// add dependency injection for all other repositories

builder.Services.AddScoped<IChargingStationRepository, ChargingStationRepository>();
builder.Services.AddScoped<IAddressRepository, AddressRepository>();
builder.Services.AddScoped<IOwnerRepository, OwnerRepository>();
builder.Services.AddScoped<IPostRepository, PostRepository>();
builder.Services.AddScoped<IClientRepository, ClientRepository>();
builder.Services.AddScoped<IReservationRepository, ReservationRepository>();

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseForwardedHeaders();


using (var scope = app.Services.CreateScope())
{
    var db = scope.ServiceProvider.GetRequiredService<ChargingStationContext>();
    Console.WriteLine("AUTOMATIC MIGRATION");
    db.Database.Migrate();
}

app.UseCors();

app.UseAuthorization();

app.UsePathBase(Environment.GetEnvironmentVariable("ASPNETCORE_PATH_BASE"));
app.UseRouting();
app.MapControllers();

app.UseSwagger(options =>
{
    options.PreSerializeFilters.Add((swaggerDoc, httpReq) =>
    {
        var proto = httpReq.Headers["X-Forwarded-Proto"].FirstOrDefault(httpReq.Scheme);
        var pathBase = Environment.GetEnvironmentVariable("ASPNETCORE_PATH_BASE");

        Console.WriteLine($"pathBase: {pathBase}");

        swaggerDoc.Servers.Clear();
        swaggerDoc.Servers.Add(new OpenApiServer { Url = $"{proto}://{httpReq.Host.Value}{pathBase}" });
    });
});
app.UseSwaggerUI();

app.Run();
