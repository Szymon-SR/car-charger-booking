import { BrowserRouter, Routes, Route } from "react-router-dom";

import "./App.css";
import MainView from "./views/Main";
import About from "./views/About";
import Reservations from "./views/Reservations";
import StationView from "./views/Station";
import OwnerView from "./views/Owner";
import OwnerEdit from "./views/OwnerEdit";
import { useEffect, useState } from "react";
import Login from "./components/login/Login";
import Register from "./components/login/Register";

function App() {
  const [lat, setLat] = useState(0);
  const [lon, setLon] = useState(0);

  useEffect(() => {
    // try to get user token form local storage
    const token = localStorage.getItem("token");
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const { latitude, longitude } = position.coords;
          console.log("Latitude:", latitude);
          setLat(latitude);
          setLon(longitude);
        },
        (error) => {
          console.error("Geolocation error:", error);
        }
      );
    } else {
      console.error("Geolocation is not supported by this browser.");
    }
  }, []);

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<MainView position={{ lat, lon }} />} />
        <Route path="/about" element={<About />} />
        <Route path="/reservations" element={<Reservations />} />
        <Route path="/station/:stationId" element={<StationView />} />
        <Route path="/owner/" element={<OwnerView />} />
        <Route path="/owner/edit" element={<OwnerEdit />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
