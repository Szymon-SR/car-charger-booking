import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import { useCallback, useEffect, useState } from "react";

import ChargingStation from "../../models/ChargingStation";
import { Stack, Divider, Box, List, ListItem, ListItemText, Typography } from "@mui/material"
import TextField from '@mui/material/TextField';
import DetailMap from "../map/DetailMap";
import { useBaseChargers } from "../../hooks/useChargers";


export default function StationDetailStack(props: any) {
  const navigate = useNavigate();

  // get station from api
  let { stationId } = useParams();
  console.log(stationId)
  const [station, setStation] = useState<ChargingStation>();

  const { getCharger: getStation } = useBaseChargers()

  useEffect(() => {
    if (stationId) {
      getStation(parseInt(stationId)).then(data => setStation(data));
    }
  }, [stationId])

  // TODO fix detailmap not rendering here
  return (
    <Box height={'100%'}>
      <Stack style={{ overflowY: "auto" }} height={'100%'}>
        <Typography variant='h4' sx={{ m: 2 }}>{ station?.name}</Typography>
        <Divider />
        <Typography sx={{ m: 2 }}>No. { station?.id}</Typography>
        <Divider />
        <Typography sx={{ m: 2 }}>Company: { station?.owner.name} </Typography>
        <Divider />
        <Typography sx={{ m: 2 }}>Address: {station?.address.street + ', '
                          + station?.address.postalCode + ', '
                          + station?.address.city + ', '
                          + station?.address.country
                        }</Typography>
        <Divider />   
        {station !== undefined && <DetailMap station={station}></DetailMap>}
      </Stack>
    </Box>

  )
}