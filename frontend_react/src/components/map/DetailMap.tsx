import {
  MapContainer,
  TileLayer,
  Marker,
  Popup,
  useMapEvents,
} from "react-leaflet";
import Button from "@mui/material/Button";

import "leaflet/dist/leaflet.css";
import L from "leaflet";

import GpsStation from "models/GpsStation";
import ChargingStation from "models/ChargingStation";
import { useEffect, useState } from "react";

export default function DetailMap({ station }: { station: ChargingStation }) {
  const [position, setPosition] = useState(
    L.latLng(station.address.latitude, station.address.longitude)
  );

  useEffect(() => {
    if (station) {
      setPosition(
        L.latLng(station.address.latitude, station.address.longitude)
      );
    }
  }, [station]);

  return (
    <MapContainer
      style={{ width: "100%", height: "50%" }}
      center={position}
      zoom={15}
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker
        icon={new L.Icon({ iconUrl: "../../charger.svg", iconSize: [32, 32] })}
        key={station.id}
        position={[station.address.latitude, station.address.longitude]}
      >
        <Popup>
          <Button variant="contained">Book</Button>
        </Popup>
      </Marker>
    </MapContainer>
  );
}
