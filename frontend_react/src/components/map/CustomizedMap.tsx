import {
  MapContainer,
  TileLayer,
  Marker,
  Popup,
  useMapEvents,
} from "react-leaflet";
import Button from "@mui/material/Button";
import { Icon } from "@mui/material";

import "leaflet/dist/leaflet.css";
import L from "leaflet";

import { useNavigate } from "react-router-dom";

import GpsStation from "models/GpsStation";
import { SetStateAction, useState } from "react";

export default function CustomizedMap(props: any) {
  // This map is used for main page, has all chargers and more options
  const navigate = useNavigate();

  // how to get stored user location in App.tsx?

  const defaultPosition = L.latLng(51.1, 17.0);

  function LocationMarker() {
    const [position, setPosition] = useState(null);

    // Define a custom icon
    const carIcon = new L.Icon({
      iconUrl: "car.svg",
      iconSize: [32, 32],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41],
    });

    // get user location on init load and on click
    const map = useMapEvents({
      click() {
        map.locate();
      },
      locationfound(e) {
        setPosition(e.latlng as unknown as SetStateAction<null>);
        map.flyTo(e.latlng, map.getZoom());
      },
    });

    return position === null ? null : (
      // create a svg icon
      <Marker position={position} icon={carIcon}>
        <Popup>You are here</Popup>
      </Marker>
    );
  }

  return (
    <MapContainer
      style={{ width: "100%", height: "100%" }}
      center={defaultPosition}
      zoom={12}
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {props.chargers.map((charger: GpsStation) => (
        <Marker
          icon={new L.Icon({ iconUrl: "charger.svg", iconSize: [32, 32] })}
          key={charger.cs.id}
          position={[charger.cs.address.latitude, charger.cs.address.longitude]}
        >
          <Popup>
            {charger.cs.name} <br /> {charger.distanceFromUser.toFixed(2)} km
            away <br />
            <Button
              onClick={() => navigate(`/station/${charger.cs.id}`)}
              variant="contained"
            >
              Book
            </Button>
          </Popup>
        </Marker>
      ))}
      <LocationMarker />
    </MapContainer>
  );
}
