import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";

import ChargingStation from "../../models/ChargingStation";
import {
  Stack,
  Divider,
  Box,
  List,
  ListItem,
  Typography,
  Grid,
  Button,
  IconButton,
  Modal,
  Checkbox,
  Input,
  FormControlLabel
} from "@mui/material";
import TextField from "@mui/material/TextField";
import Owner from "models/Owner";
import jwt_decode from "jwt-decode";
import { useBaseOwners } from "../../hooks/useOwners";
import { ChargerAndAddressForm } from "models/Forms";
import { useForm, SubmitHandler } from "react-hook-form";

const modalStyle = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};


export default function OwnerDetail(props: any) {
  const navigate = useNavigate();

  // get station from api

  //get email stored in local storage in jwt token
  const token = localStorage.getItem("token");
  // decode token
  const decoded: any = token ? jwt_decode(token) : null;
  const email = decoded ? decoded.email : null;

  const [owner, setOwner] = useState<Owner>();

  const { getOwner: getOwner } = useBaseOwners();

  // modal to add chargers
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  // form to add chargers
  const { register, handleSubmit, watch, formState: { errors } } = useForm<ChargerAndAddressForm>();
  const onSubmit: SubmitHandler<ChargerAndAddressForm> = data => console.log(data);

  useEffect(() => {
    const storedOwner = localStorage.getItem("owner");
    if (storedOwner) {
      const owner = JSON.parse(storedOwner);
      setOwner(owner);
    }
  }, []);

  return (
    <Box style={{ padding: "3rem" }}>
      <Grid container spacing={2}>
        <Grid item xs={12} lg={5}>
          <Box>
            <Stack style={{ overflowY: "auto" }}>
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <Typography variant="h4" sx={{ m: 2 }}>
                  {owner?.name}
                </Typography>
                <IconButton onClick={() => navigate(`/owner/edit`)} style={{}}>
                  <Typography variant="h6">Edit</Typography>
                </IconButton>
              </div>
              <Divider />
              <Typography sx={{ m: 2 }}>{owner?.phone}</Typography>
              <Divider />
              <Typography sx={{ m: 2 }}>{owner?.email}</Typography>
              <Divider />
              <Typography sx={{ m: 2 }}>{owner?.description}</Typography>
              <Divider />
            </Stack>
          </Box>
        </Grid>
        <Grid item xs={12} lg={7}>
          <Box>
            <Typography variant="h4" sx={{ m: 2 }}>
              Your Chargers
            </Typography>
            <Divider />
            <Box>
              <Button variant="contained" onClick={handleOpen}>Add new</Button>
            </Box>
            <Modal
              open={open}
              onClose={handleClose}
              aria-labelledby="modal-modal-title"
              aria-describedby="modal-modal-description"
            >
              <Box sx={modalStyle}>
                <form onSubmit={handleSubmit(onSubmit)}>
                  <Stack spacing={1}>
                  <Typography variant="h5">Input new charger data</Typography>
                  <TextField required label="Name" {...register("name")} />
                  <TextField required label="Description" {...register("description")} />
                  <TextField required label="Type" {...register("chargerType")} />
                  <FormControlLabel control={<Checkbox  {...register("active")} />} label="Active" />
                  <Divider />
                  <TextField required label="Street" {...register("street")} />
                  <TextField required label="City" {...register("city")} />
                  <TextField required label="Postal Code" {...register("postalCode")} />
                  <TextField required label="Country" {...register("country")} />
                  <TextField required label="Latitude" {...register("latitude")} />
                  <TextField required label="Longitude" {...register("longitude")} />
                  <Input type="submit" />
                  </Stack>
                </form>
              </Box>
            </Modal>

            <List>
              {owner?.chargingStations.map((station: ChargingStation) => (
                <ListItem
                  key={station.id}
                  button
                  onClick={() => navigate(`/station/${station.id}`)}
                  style={{
                    padding: "10px",
                    display: "flex",
                    justifyContent: "space-between",
                  }}
                >
                  <Stack style={{ overflowY: "auto" }}>
                    <Typography>STATION NO. {station.id}</Typography>
                    <Typography variant="h5" style={{ color: "green" }}>
                      {station.name}
                    </Typography>
                    <Typography>
                      {station.address.street +
                        ", " +
                        station.address.postalCode +
                        ", " +
                        station.address.city +
                        ", " +
                        station.address.country}
                    </Typography>
                    <Typography>{station.description}</Typography>
                    <Typography>{station.active}</Typography>
                  </Stack>
                  <IconButton
                    onClick={() => navigate(`/station/edit/${station.id}`)}
                  >
                    <Typography variant="h5">Edit</Typography>
                  </IconButton>
                </ListItem>
              ))}
            </List>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
}
