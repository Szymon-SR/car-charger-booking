import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import { useCallback, useEffect, useState } from "react";

import ChargingStation from "../../models/ChargingStation";
import {
  Stack,
  Divider,
  Box,
  List,
  ListItem,
  ListItemText,
  Typography,
  Grid,
  Button,
  IconButton,
} from "@mui/material";
import TextField from "@mui/material/TextField";
import DetailMap from "../map/DetailMap";
import { useBaseChargers } from "../../hooks/useChargers";
import Owner from "models/Owner";
import { useBaseOwners } from "../../hooks/useOwners";
import jwt_decode from "jwt-decode";
import OwnerDTO from "models/OwnerDTO";

export default function OwnerEditView(props: any) {
  const navigate = useNavigate();

  const [password, setPassword] = useState<string>("");
  const token = localStorage.getItem("token");
  // decode token
  const decoded: any = token ? jwt_decode(token) : null;
  const email = decoded ? decoded.email : null;

  // get station from api
  const [owner, setOwner] = useState<Owner>();

  const { getOwner, updateOwner } = useBaseOwners();

  useEffect(() => {
    const storedOwner = localStorage.getItem("owner");
    if (storedOwner) {
      const owner = JSON.parse(storedOwner);
      setOwner(owner);
    }
  }, []);

  const handleFormSubmit = useCallback(
    (event: any) => {
      event.preventDefault();
      const name = event.target.elements.name.value;
      const email = event.target.elements.email.value;
      const phone = event.target.elements.phone.value;
      const description = event.target.elements.description.value;
      const updatedOwner: OwnerDTO = {
        id: owner?.id,
        name: name,
        email: email,
        password: password,
        phone: phone,
        description: description,
      };
      console.log(owner);

      // use postOwner from useBaseOwners
      updateOwner(owner.id, updatedOwner).then(
        (data) => {
          console.log(data);
          navigate(`/owner`);
        },
        (error) => {
          console.log(error);
        }
      );
    },
    [owner, updateOwner, navigate]
  );

  if (!owner) {
    return <div>Loading...</div>;
  }

  return (
    <Box style={{ padding: "3rem" }}>
      <Typography variant="h5">Owner Edit</Typography>
      <form onSubmit={handleFormSubmit}>
        <Stack spacing={2} sx={{ mt: 2 }}>
          <TextField
            id="name"
            label="Name"
            variant="outlined"
            fullWidth
            defaultValue={owner?.name}
          />
          <TextField
            id="email"
            label="Email"
            variant="outlined"
            fullWidth
            defaultValue={owner?.email}
          />
          <TextField
            label="Password"
            variant="outlined"
            type="password"
            onChange={(event) => setPassword(event.target.value)}
          />
          <TextField
            id="phone"
            label="Phone"
            variant="outlined"
            fullWidth
            defaultValue={owner?.phone}
          />
          <TextField
            id="description"
            label="Description"
            variant="outlined"
            fullWidth
            multiline
            rows={4}
            defaultValue={owner?.description}
          />
          <Button type="submit" variant="contained" color="primary">
            Save
          </Button>
        </Stack>
      </form>
    </Box>
  );
}
