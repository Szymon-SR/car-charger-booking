import React, { useEffect, useState } from "react";
import {
  Button,
  TextField,
  Typography,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import ChargerType from "../../models/ChargerType";
import { useBaseClients } from "../../hooks/useClients";
import { useBaseOwners } from "../../hooks/useOwners";
import ClientDTO from "../../models/ClientDTO";
import OwnerDTO from "../../models/OwnerDTO";

const Register = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [ownerPassword, setOwnerPassword] = useState("");
  const [accountType, setAccountType] = useState("client"); // Default to owner account type
  const [ownerName, setOwnerName] = useState("");
  const [ownerEmail, setOwnerEmail] = useState("");
  const [ownerPhone, setOwnerPhone] = useState("");
  const [ownerDescription, setOwnerDescription] = useState("");
  const [clientFirstName, setClientFirstName] = useState("");
  const [clientLastName, setClientLastName] = useState("");
  const [clientPhone, setClientPhone] = useState("");
  const [selectedChargerType, setSelectedChargerType] = useState(
    ChargerType.Type1
  );

  const { createClient } = useBaseClients();
  const { createOwner } = useBaseOwners();

  const handleSelectChange = (event) => {
    setSelectedChargerType(event.target.value);
  };

  const handleRegister = () => {
    if (accountType === "client") {
      const clientDTO: ClientDTO = {
        email: email,
        password: password,
        firstName: clientFirstName,
        lastName: clientLastName,
        phone: clientPhone,
        chargerType: selectedChargerType,
      };
      console.log({ clientDTO });
      createClient(clientDTO)
        .then((response) => {
          // Handle successful client creation
          console.log("Client created:", response);
        })
        .catch((error) => {
          // Handle client creation error
          console.error("Client creation error:", error);
        });
    } else {
      const ownerDTO: OwnerDTO = {
        name: ownerName,
        email: ownerEmail,
        password: ownerPassword,
        phone: ownerPhone,
        description: ownerDescription,
      };
      console.log({ ownerDTO });
      createOwner(ownerDTO)
        .then((response) => {
          // Handle successful owner creation
          console.log("Owner created:", response);
        })
        .catch((error) => {
          // Handle owner creation error
          console.error("Owner creation error:", error);
        });
    }
  };

  const handleAccountTypeChange = (event) => {
    const newAccountType = event.target.value;
    setAccountType(newAccountType);

    // Reset all fields
    setEmail("");
    setPassword("");
    setOwnerName("");
    setOwnerEmail("");
    setOwnerPassword("");
    setOwnerPhone("");
    setOwnerDescription("");
    setClientFirstName("");
    setClientLastName("");
    setClientPhone("");
    setSelectedChargerType(ChargerType.Type1);
  };

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        minHeight: "100vh",
      }}
    >
      <Typography variant="h4" component="h2">
        Register
      </Typography>
      <Typography variant="h6" component="h2">
        Select Account Type
      </Typography>
      <RadioGroup
        aria-label="account-type"
        name="account-type"
        value={accountType}
        onChange={handleAccountTypeChange}
        style={{ margin: "10px" }}
      >
        <FormControlLabel
          value="client"
          control={<Radio />}
          label="Client Account"
        />
        <FormControlLabel
          value="owner"
          control={<Radio />}
          label="Owner Account"
        />
      </RadioGroup>
      {accountType === "client" ? (
        <>
          <TextField
            label="First Name"
            variant="outlined"
            onChange={(event) => setClientFirstName(event.target.value)}
            style={{ margin: "10px" }}
          />
          <TextField
            label="Last Name"
            variant="outlined"
            onChange={(event) => setClientLastName(event.target.value)}
            style={{ margin: "10px" }}
          />
          <TextField
            label="Email"
            variant="outlined"
            type="email"
            onChange={(event) => setEmail(event.target.value)}
            style={{ margin: "10px" }}
          />
          <TextField
            label="Password"
            variant="outlined"
            type="password"
            onChange={(event) => setPassword(event.target.value)}
            style={{ margin: "10px" }}
          />
          <TextField
            label="Phone"
            variant="outlined"
            onChange={(event) => setClientPhone(event.target.value)}
            style={{ margin: "10px" }}
          />
          <FormControl variant="outlined" style={{ margin: "10px" }}>
            <InputLabel id="chargerTypeLabel">Charger Type</InputLabel>
            <Select
              labelId="chargerTypeLabel"
              id="chargerType"
              value={selectedChargerType}
              onChange={handleSelectChange}
              label="Charger Type"
            >
              {Object.values(ChargerType).map((charger) => (
                <MenuItem key={charger} value={charger}>
                  {charger}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </>
      ) : (
        <>
          <TextField
            label="Name"
            variant="outlined"
            onChange={(event) => setOwnerName(event.target.value)}
            style={{ margin: "10px" }}
          />
          <TextField
            label="Email"
            variant="outlined"
            type="email"
            onChange={(event) => setOwnerEmail(event.target.value)}
            style={{ margin: "10px" }}
          />
          <TextField
            label="Password"
            variant="outlined"
            type="password"
            onChange={(event) => setOwnerPassword(event.target.value)}
            style={{ margin: "10px" }}
          />
          <TextField
            label="Phone"
            variant="outlined"
            type="phone"
            onChange={(event) => setOwnerPhone(event.target.value)}
            style={{ margin: "10px" }}
          />
          <TextField
            label="Description"
            multiline
            type="description"
            variant="outlined"
            onChange={(event) => setOwnerDescription(event.target.value)}
            style={{ margin: "10px" }}
          />
        </>
      )}
      <Button
        variant="contained"
        color="secondary"
        onClick={handleRegister}
        style={{ margin: "10px" }}
      >
        Register
      </Button>
    </div>
  );
};

export default Register;
