import React, { useEffect, useState } from "react";
import {
  Button,
  TextField,
  Typography,
  Radio,
  RadioGroup,
  FormControlLabel,
} from "@mui/material";
import jwt_decode from "jwt-decode";
import { useNavigate } from "react-router-dom";
import { useBaseClients } from "../../hooks/useClients";
import { useBaseOwners } from "../../hooks/useOwners";
import Client from "../../models/Client";
import Owner from "../../models/Owner";
import ClientDTO from "../../models/ClientDTO";
import ChargerType from "../../models/ChargerType";
import OwnerDTO from "../../models/OwnerDTO";

const clientId =
  "115754339445-pq5g2h7uhdm8v8am14e7iimorvvochop.apps.googleusercontent.com";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [accountType, setAccountType] = useState("client"); // Default to client account type
  const [owner, setOwner] = useState<Owner>();
  const { getOwner, loginOwner } = useBaseOwners();
  const [client, setClient] = useState<Client>();
  const { getClientByEmail, loginClient } = useBaseClients();
  const [message, setMessage] = useState("");

  const handleLogin = () => {
    if (accountType === "client") {
      const clientDTO: ClientDTO = {
        email: email,
        password: password,
        firstName: "",
        lastName: "",
        phone: "",
        chargerType: ChargerType.Type1,
      };
      console.log({ clientDTO });
      loginClient(clientDTO)
        .then((response) => {
          // Handle successful client creation
          console.log("Client logged:", response);
          localStorage.setItem("client", JSON.stringify(response));
          localStorage.removeItem("owner");
          navigate("/");
        })
        .catch((error) => {
          // Handle client creation error
          console.error("Client login error:", error);
          setMessage(error.message);
        });
    } else {
      const ownerDTO: OwnerDTO = {
        name: "",
        email: email,
        password: password,
        phone: "",
        description: "",
      };
      console.log({ ownerDTO });
      loginOwner(ownerDTO)
        .then((response) => {
          // Handle successful owner creation
          console.log("Owner logged:", response);
          localStorage.setItem("owner", JSON.stringify(response));
          localStorage.removeItem("client");
          navigate("/");
        })
        .catch((error) => {
          // Handle owner creation error
          console.error("Owner login error:", error);
          setMessage(error.message);
        });
    }
  };

  const handleAccountTypeChange = (event) => {
    setAccountType(event.target.value);
  };

  const [user, setUser] = useState({});

  const fetchUserByEmail = async (email: string) => {
    console.log(accountType);
    if (accountType === "client") {
      // catch error if getClientByEmail returns error
      try {
        const data = await getClientByEmail(email);
        setClient(data);
        localStorage.setItem("client", JSON.stringify(data));
        localStorage.removeItem("owner");
        navigate("/");
      } catch (error) {
        // if error, navigate to register page
        navigate("/register");
      }
    } else if (accountType === "owner") {
      try {
        const data = await getOwner(email);
        setOwner(data);
        localStorage.setItem("owner", JSON.stringify(data));
        localStorage.removeItem("client");
        navigate("/");
      } catch (error) {
        // if error, navigate to register page
        navigate("/register");
      }
    }
  };

  function handleCallbackResponse(response: any) {
    console.log("Encoded JWT ID token: " + response.credential);
    var userObj = jwt_decode(response.credential);
    console.log(userObj);

    localStorage.setItem("token", response.credential);

    setUser(userObj);
    document.getElementById("signInDiv").hidden = true;
    fetchUserByEmail(userObj.email);
  }

  function handleSignOut(event: any) {
    setUser({});
    document.getElementById("signInDiv").hidden = false;
    // remove token from local storage
    localStorage.removeItem("token");
  }

  useEffect(() => {
    google.accounts.id.initialize({
      client_id: clientId,
      callback: handleCallbackResponse,
    });
    if (localStorage.getItem("token")) {
      var userObj = jwt_decode(localStorage.getItem("token"));
      setUser(userObj);
      document.getElementById("signInDiv").hidden = true;
    }
    google.accounts.id.renderButton(document.getElementById("signInDiv"), {
      theme: "outline",
      size: "large",
    });
  }, [accountType]);

  const navigate = useNavigate();

  useEffect(() => {
    console.log({ client });
  }, [client]);
  // if client or owner is logged in, redirect to main page

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        minHeight: "100vh",
      }}
    >
      <Typography variant="h4" component="h2">
        Login
      </Typography>
      <Typography variant="h6" component="h2">
        Select Account Type
      </Typography>
      <RadioGroup
        aria-label="account-type"
        name="account-type"
        value={accountType}
        onChange={handleAccountTypeChange}
        style={{ margin: "10px" }}
      >
        <FormControlLabel
          value="client"
          control={<Radio />}
          label="Client Account"
        />
        <FormControlLabel
          value="owner"
          control={<Radio />}
          label="Owner Account"
        />
      </RadioGroup>
      <Typography variant="h6" component="h2">
        Sign In using Google
      </Typography>
      <div id="signInDiv"></div>
      {Object.keys(user).length !== 0 && (
        <button className="sign-out-button" onClick={(e) => handleSignOut(e)}>
          Sign Out
        </button>
      )}
      <Typography variant="h6" component="h2">
        Or use email and password
      </Typography>
      {user && <div id="loggedUser">{user.name}</div>}
      <TextField
        label="Email"
        variant="outlined"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        style={{ margin: "10px" }}
      />
      {message !== "" && <div id="message">{message}</div>}
      <TextField
        label="Password"
        variant="outlined"
        type="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        style={{ margin: "10px" }}
      />
      <Button
        variant="contained"
        color="primary"
        onClick={handleLogin}
        style={{ margin: "10px" }}
      >
        Login
      </Button>
      <Button
        variant="contained"
        color="secondary"
        onClick={() => navigate("/register")}
        style={{ margin: "10px" }}
      >
        Register
      </Button>
    </div>
  );
};

export default Login;
