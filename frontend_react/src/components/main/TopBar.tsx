import FaceIcon from "@mui/icons-material/Face";
import EnergyIcon from "@mui/icons-material/ElectricBoltTwoTone";
import { useNavigate } from "react-router-dom";

import { Stack } from "@mui/system";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Grid,
  Chip,
  Box,
} from "@mui/material";
import { useEffect, useState } from "react";
import jwt_decode from "jwt-decode";

const clientId =
  "115754339445-pq5g2h7uhdm8v8am14e7iimorvvochop.apps.googleusercontent.com";

const TopBar = () => {
  const [user, setUser] = useState({});

  function handleCallbackResponse(response: any) {
    console.log("Encoded JWT ID token: " + response.credential);
    var userObj = jwt_decode(response.credential);
    console.log(userObj);

    localStorage.setItem("token", response.credential);

    setUser(userObj);
    document.getElementById("signInDiv").hidden = true;
  }

  function handleSignOut(event: any) {
    setUser({});
    document.getElementById("signInDiv").hidden = false;
    // remove token from local storage
    localStorage.removeItem("token");
    // remoe client from local storage
    localStorage.removeItem("client");
    // remove owner from local storage
    localStorage.removeItem("owner");
  }

  useEffect(() => {
    google.accounts.id.initialize({
      client_id: clientId,
      callback: handleCallbackResponse,
    });
    if (localStorage.getItem("token")) {
      var userObj = jwt_decode(localStorage.getItem("token"));
      setUser(userObj);
      document.getElementById("signInDiv").hidden = true;
    }
    google.accounts.id.renderButton(document.getElementById("signInDiv"), {
      theme: "outline",
      size: "large",
    });
  }, []);

  const navigate = useNavigate();

  return (
    <div>
      <AppBar position="fixed" color="primary">
        <Toolbar variant="dense">
          <Box sx={{ display: "flex", flexWrap: "wrap", gap: "12px" }}>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ mr: 2 }}
              onClick={() => navigate("/")}
            >
              <EnergyIcon name="zap" />
            </IconButton>
            <Typography variant="h6" color="inherit" component="div">
              Car Chargers
            </Typography>
            <Chip
              icon={<FaceIcon />}
              onClick={() => navigate("/reservations")}
              label="Reservations"
              variant="filled"
              color="secondary"
            />
            <Chip
              icon={<FaceIcon />}
              onClick={() => navigate("/about")}
              label="About"
              variant="filled"
              color="secondary"
            />
            <Chip
              icon={<FaceIcon />}
              onClick={() => navigate("/owner")}
              label="Owner"
              variant="filled"
              color="secondary"
            />
            <Chip
              icon={<FaceIcon />}
              onClick={() => navigate("/login")}
              label="Login"
              variant="filled"
              color="secondary"
            />
            <div id="signInDiv"></div>
            {Object.keys(user).length != 0 && (
              <button
                className="sign-out-button"
                onClick={(e) => handleSignOut(e)}
              >
                Sign Out
              </button>
            )}
            {user && (
              <div id="loggedUser">
                <img src={user.picture} width="30px" />
                {user.name}
              </div>
            )}
          </Box>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default TopBar;
