import { useNavigate } from "react-router-dom";

import GpsStation from "models/GpsStation";
import { Stack, Box, Button, List, ListItem, ListItemText, Divider, IconButton, Typography, Badge } from "@mui/material"
import TextField from '@mui/material/TextField';
import { useState } from "react";

export default function SideBar(props: any) {
  const navigate = useNavigate();

  // outlined-basic is the id of the text field it is used to filter the chargers by name, address, description, etc.

  const [searchText, setSearchText] = useState('');

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value);
  };

  // Filter the chargers based on the search text
  const filteredChargers = props.chargers.filter((charger: GpsStation) => {
    console.log(charger);
    const searchString = searchText.toLowerCase();
    const chargerName = charger.cs.name.toLowerCase();
    const chargerAddress = charger.cs.address.street.toLowerCase() +
      charger.cs.address.postalCode.toLowerCase() +
      charger.cs.address.city.toLowerCase() +
      charger.cs.address.country.toLowerCase();
    const chargerDescription = charger.cs.description.toLowerCase();
    const chargerType = charger.cs.chargerType.toLowerCase();

    return (
      chargerName.includes(searchString) ||
      chargerAddress.includes(searchString) ||
      chargerDescription.includes(searchString) ||
      chargerType.includes(searchString)
    );
  });


  return (
    <Stack>
      <Box>
        <Stack mt={1} mb={1} spacing={1}>
          <h2>Search chargers</h2>
          <TextField
            id="outlined-basic"
            label="Input any place"
            variant="outlined"
            value={searchText}
            onChange={handleSearchChange}
          />
        </Stack>
      </Box>
      <Box>
        <Stack mt={1} mb={1} spacing={1} >
          <h2>Nearby chargers</h2>
          <List sx={{
            width: '100%',
            height: '100%',
            overflow: 'auto',
          }}>
            {filteredChargers.map((charger: GpsStation) => (
              <ListItem key={charger.cs.id} button onClick={() => navigate(`/station/${charger.cs.id}`)} style={{ padding: '10px', display: 'flex', justifyContent: 'space-between' }}>
                <Stack style={{ overflowY: "auto" }}>
                  <div style={{display: "flex", position: "relative"}}>
                  <Typography variant="h5" style={{ color: "green" }}>{charger.cs.name}</Typography>
                  
                  {charger.cs.active ? (
                    <Badge color="success" variant="dot">
                      active
                    </Badge>
                    ) : 
                      <Badge color="error" variant="dot">
                      not active
                    </Badge>
                  }
                  </div>
                  <Typography>
                    {charger.cs.address.street + ', '
                      + charger.cs.address.postalCode + ', '
                      + charger.cs.address.city + ', '
                      + charger.cs.address.country
                    }</Typography>
                  <Typography>{charger.distanceFromUser} km. from your position.</Typography>
                  <ListItemText primary={charger.cs.chargerType}></ListItemText>
                  
                </Stack>
                <Button onClick={() => navigate(`/station/${charger.cs.id}`)} variant="contained">Book</Button>
              </ListItem>
            ))}
          </List>
        </Stack>
      </Box>
    </Stack>
  )
}