import axios from "axios";
import { API_BASE_IP } from "../constants";
import Owner from "models/Owner";
import OwnerDTO from "models/OwnerDTO";

export const useBaseOwners = () => {
  const getOwner = (email: string) =>
    axios
      .get<Owner>(`${API_BASE_IP}/owners/${email}`)
      .then((response) => response.data)
      .catch((error) => {
        throw new Error(error.response.data);
      });

  const getOwners = () =>
    axios
      .get<Owner[]>(`${API_BASE_IP}/owners`)
      .then((response) => response.data)
      .catch((error) => {
        throw new Error(error.response.data);
      });

  const createOwner = (owner: OwnerDTO) =>
    axios
      .post<Owner>(`${API_BASE_IP}/owners`, owner)
      .then((response) => response.data)
      .catch((error) => {
        throw new Error(error.response.data);
      });

  const updateOwner = (id: number, owner: OwnerDTO) =>
    axios
      .put<Owner>(`${API_BASE_IP}/owners/${id}`, owner)
      .then((response) => response.data)
      .catch((error) => {
        throw new Error(error.response.data);
      });

  const deleteOwner = (id: number) =>
    axios
      .delete(`${API_BASE_IP}/owners/${id}`)
      .then((response) => response.data)
      .catch((error) => {
        throw new Error(error.response.data);
      });
  
  const loginOwner = (owner: OwnerDTO) =>
    axios
      .post<Owner>(`${API_BASE_IP}/owners/login`, owner)
      .then((response) => response.data)
      .catch((error) => {
        throw new Error(error.response.data);
      });
  

  return {
    getOwner,
    getOwners,
    createOwner,
    updateOwner,
    deleteOwner,
    loginOwner,
  };
};
