import axios from "axios";
import { API_BASE_IP } from "../constants";
import Reservation from "models/Reservation";
import ReservationDTO from "models/ReservationDTO";

export const useBaseReservations = () => {
  const getReservation = (id: number) =>
    axios.get<Reservation>(`${API_BASE_IP}/reservations/${id}`).then((response) => response.data);


  const getReservations = () =>
    axios.get<Reservation[]>(`${API_BASE_IP}/reservations`).then((response) => response.data);

  const createReservation = (post: ReservationDTO) =>
    axios.post<Reservation>(`${API_BASE_IP}/reservations`, post).then((response) => response.data);

  const updateReservation = (id: number, post: ReservationDTO) =>
    axios.put<Reservation>(`${API_BASE_IP}/reservations/${id}`, post).then((response) => response.data);

  const deleteReservation = (id: number) =>
    axios.delete(`${API_BASE_IP}/reservations/${id}`).then((response) => response.data);

  return {
    getReservation,
    getReservations,
    createReservation,
    updateReservation,
    deleteReservation,
  };
};
