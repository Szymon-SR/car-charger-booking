import axios from "axios";
import { API_BASE_IP } from "../constants";
import Post from "models/Post";
import PostDTO from "models/PostDTO";

export const useBasePosts = () => {
  const getPost = (id: number) =>
    axios.get<Post>(`${API_BASE_IP}/posts/${id}`).then((response) => response.data);


  const getPosts = () =>
    axios.get<Post[]>(`${API_BASE_IP}/posts`).then((response) => response.data);

  const createPost = (post: PostDTO) =>
    axios.post<Post>(`${API_BASE_IP}/posts`, post).then((response) => response.data);

  const updatePost = (id: number, post: PostDTO) =>
    axios.put<Post>(`${API_BASE_IP}/posts/${id}`, post).then((response) => response.data);

  const deletePost = (id: number) =>
    axios.delete(`${API_BASE_IP}/posts/${id}`).then((response) => response.data);

  return {
    getPost,
    getPosts,
    createPost,
    updatePost,
    deletePost,
  };
};
