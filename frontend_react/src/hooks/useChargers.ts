import axios from "axios";
import ChargingStation from "../models/ChargingStation";

import { API_BASE_IP } from "../constants";
import GpsStation from "models/GpsStation";

export const useGpsChargers = () => {

  const getGpsCharger = (id: number, lat: number, lon: number) =>
    axios
      .get<GpsStation>(`${API_BASE_IP}/gps/${id}?latitude=${lat}&longitude=${lon}`)
      .then(response => response.data);


  const getGpsChargers = (lat: number, lon: number) =>
    axios
      .get<GpsStation[]>(`${API_BASE_IP}/gps?latitude=${lat}&longitude=${lon}`)
      .then(response => response.data);

  return {
    getGpsCharger: getGpsCharger,
    getGpsChargers: getGpsChargers,
  };
};

export const useBaseChargers = () => {
  const getCharger = (id: number) =>
    axios
      .get<ChargingStation>(`${API_BASE_IP}/stations/${id}`)
      .then(response => response.data);


  const getChargers = () =>
    axios
      .get<ChargingStation[]>(`${API_BASE_IP}/stations`)
      .then(response => response.data);

  return {
    getCharger: getCharger,
    getChargers: getChargers,
  };
};
