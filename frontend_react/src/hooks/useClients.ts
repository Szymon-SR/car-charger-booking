import axios from "axios";
import { API_BASE_IP } from "../constants";
import Client from "models/Client";
import ClientDTO from "models/ClientDTO";

export const useBaseClients = () => {
  const getClient = (id: number) =>
    axios
      .get<Client>(`${API_BASE_IP}/clients/${id}`)
      .then((response) => response.data)
      .catch((error) => {
        throw new Error(error.response.data);
      });

  const getClients = () =>
    axios
      .get<Client[]>(`${API_BASE_IP}/clients`)
      .then((response) => response.data)
      .catch((error) => {
        throw new Error(error.response.data);
      });

  const createClient = (client: ClientDTO) =>
    axios
      .post<Client>(`${API_BASE_IP}/clients`, client)
      .then((response) => response.data)
      .catch((error) => {
        throw new Error(error.response.data);
      });

  const updateClient = (id: number, client: ClientDTO) =>
    axios
      .put<Client>(`${API_BASE_IP}/clients/${id}`, client)
      .then((response) => response.data)
      .catch((error) => {
        throw new Error(error.response.data);
      });

  const deleteClient = (id: number) =>
    axios
      .delete(`${API_BASE_IP}/clients/${id}`)
      .then((response) => response.data)
      .catch((error) => {
        throw new Error(error.response.data);
      });

  const getClientByEmail = (email: string) =>
    axios
      .get<Client>(`${API_BASE_IP}/clients/email/${email}`)
      .then((response) => response.data)
      .catch((error) => {
        throw new Error(error.response.data);
      });
  
  const loginClient = (client: ClientDTO) =>
    axios
      .post<Client>(`${API_BASE_IP}/clients/login`, client)
      .then((response) => response.data)
      .catch((error) => {
        throw new Error(error.response.data);
      });
  

  return {
    getClient,
    getClients,
    createClient,
    updateClient,
    deleteClient,
    getClientByEmail,
    loginClient,
  };
};
