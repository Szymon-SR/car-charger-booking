import Owner from "./Owner";

interface ChargingStation {
    id: number;
    owner: Owner;
    name: string;
    address: Address;
    description: string;
    active: boolean;
    chargerType: string;
  }

export default ChargingStation;