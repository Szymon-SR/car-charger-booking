interface Client {
    id: number;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    phone?: string;
    chargerType?: string;
  }
  
export default Client;