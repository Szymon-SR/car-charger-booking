export type ChargerAndAddressForm = {
  "name": string,
  "description": string,
  "active": boolean,
  "chargerType": string,
  //address
  "street": string,
  "city": string,
  "postalCode": string,
  "country": string,
  "latitude": number,
  "longitude": number
}
