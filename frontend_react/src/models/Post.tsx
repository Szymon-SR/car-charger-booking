import ChargingStation from "./ChargingStation";

interface Post {
    id: number;
    chargingStation: ChargingStation;
    client: Client;
    content: string;
    startDate: Date;
    stars: number;
    car?: string | null;
  }

export default Post;