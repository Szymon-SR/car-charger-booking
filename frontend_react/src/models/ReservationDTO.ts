interface ReservationDTO {
  id?: number;
  chargingStationId: number;
  clientId?: number;
  startDate: Date;
  endDate: Date;
  confirmed: boolean;
  active: boolean;
}

export default ReservationDTO;