interface ClientDTO {
  id?: number;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  phone: string;
  chargerType: string;
}

export default ClientDTO;