import ChargingStation from "./ChargingStation";

interface Owner {
  id: number;
  name: string;
  email: string;
  password: string;
  phone?: string;
  description?: string;
  chargingStations: ChargingStation[];
}

export default Owner;
