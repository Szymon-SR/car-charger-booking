interface OwnerDTO {
    id?: number;
    name: string;
    password: string;
    email: string;
    phone?: string;
    description?: string;
    chargingStationsIds?: number[];
  }

export default OwnerDTO;