import ChargingStation from "./ChargingStation";
import Client from "./Client";

interface Reservation {
  id: number;
  chargingStation: ChargingStation;
  client: Client;
  startDate: Date;
  endDate: Date;
  confirmed: boolean;
  active: boolean;
}

export default Reservation;