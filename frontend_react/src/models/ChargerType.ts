enum ChargerType {
  Type1 = "Type1",
  Type2 = "Type2",
  Type3 = "Type3",
  CCS = "CCS",
  CHAdeMO = "CHAdeMO",
}

export default ChargerType;
