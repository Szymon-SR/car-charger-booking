interface PostDTO {
  id?: number;
  content: string;
  chargingStationId: number;
  clientId: number;
  startDate: Date;
  stars: number;
  car?: string | null;
}

export default PostDTO;