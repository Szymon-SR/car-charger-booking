import ChargingStation from "./ChargingStation";

interface GpsStation {
    cs: ChargingStation;
    distanceFromUser: number;
}

export default GpsStation;