const IP_DOCKER = 'http://localhost:8001'



const IP_DEVEL = 'https://localhost:8001'
const ip = import.meta.env.VITE_APP_API_BASE_IP;
// export const IP_PROD_DOCKER = `https://${ip}:8000/api`
export const IP_PROD_DOCKER = `https://${ip}:8001`

// if vite mode development set ip to localhost else set ip to docker
// how to run react in prod???
// export const API_BASE_IP = import.meta.env.MODE === 'development' ? IP_DEVEL : IP_PROD_DOCKER;
export const API_BASE_IP = IP_PROD_DOCKER;
