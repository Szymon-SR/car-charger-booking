import {
  Typography,
  Box,
  Stack,
  Divider,
  Button,
  TextareaAutosize,
  ListItem,
} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2"; // Grid version 2
import TopBar from "../components/main/TopBar";

import StationDetailStack from "../components/stations/StationDetailStack";
import ChargingStation from "models/ChargingStation";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useBaseChargers } from "../hooks/useChargers";
import { useBasePosts } from "../hooks/usePosts";
import Post from "models/Post";
import PostDTO from "models/PostDTO";
import BookStation from "./BookStation";

const StationView = () => {
  const { stationId } = useParams();
  const stationIdInt = parseInt(stationId || "0");
  const [station, setStation] = useState<ChargingStation>();
  const [posts, setPosts] = useState<Post[]>([]);
  const [newPostContent, setNewPostContent] = useState("");
  const [newPostStars, setNewPostStars] = useState(1);
  const [newPostCar, setNewPostCar] = useState("");
  const { getCharger: getStation } = useBaseChargers();
  const { getPosts, createPost } = useBasePosts();
  const [selectedComponent, setSelectedComponent] = useState("Comments"); // New state variable
  const [clientId, setClientId] = useState(0); // New state variable

  useEffect(() => {
    if (stationId) {
      getStation(parseInt(stationId)).then((data) => setStation(data));
    }
  }, [stationId]);

  useEffect(() => {
    getPosts().then((posts) => {
      const filteredPosts = posts.filter(
        (post) => post.chargingStation.id === stationIdInt
      );
      setPosts(filteredPosts);
    });
  }, [stationId]);

  // use effatc at first render to get client stored int local storage
  useEffect(() => {
    const storedClient = localStorage.getItem("client");
    if (storedClient) {
      const client = JSON.parse(storedClient);
      setClientId(client.id);
    }
  });

  const handleSubmit = () => {
    const newPost: PostDTO = {
      content: newPostContent,
      chargingStationId: station?.id || 0,
      clientId: clientId,
      startDate: new Date(),
      stars: newPostStars,
      car: newPostCar || null,
    };

    createPost(newPost).then((data) => {
      setNewPostContent("");
      setNewPostStars(1);
      setNewPostCar("");
      getPosts().then((posts) => {
        const filteredPosts = posts.filter(
          (post) => post.chargingStation.id === stationIdInt
        );
        setPosts(filteredPosts);
      });
    });
  };

  return (
    <div style={{ height: "100%" }}>
      <TopBar />
      <Box padding={"3rem"} height={"100%"}>
        <Grid container spacing={2} height={"100%"}>
          <Grid xs={12} lg={5}>
            <StationDetailStack />
          </Grid>
          <Grid xs={12} lg={7}>
            <Box>
              <Box sx={{ m: 2 }}>
                <Button
                  variant={
                    selectedComponent === "Comments" ? "contained" : "outlined"
                  }
                  color="primary"
                  onClick={() => setSelectedComponent("Comments")}
                  sx={{ marginRight: 2 }}
                >
                  <Typography>Comments</Typography>
                </Button>
                <Button
                  variant={
                    selectedComponent === "Book" ? "contained" : "outlined"
                  }
                  color="primary"
                  onClick={() => setSelectedComponent("Book")}
                >
                  <Typography>Book</Typography>
                </Button>
              </Box>

              <Typography variant="h4" sx={{ m: 2 }}>
                {selectedComponent === "Comments" ? "Comments" : "Book"}
              </Typography>
              <Divider />

              {/* Add the form for creating a new post */}
              {selectedComponent === "Comments" && (
                <>
                  <Typography variant="h6" sx={{ m: 2 }}>
                    Create a New Post
                  </Typography>
                  <TextareaAutosize
                    placeholder="Content"
                    value={newPostContent}
                    onChange={(e) => setNewPostContent(e.target.value)}
                  />
                  <Box sx={{ m: 2 }}>
                    <Typography>Stars:</Typography>
                    <input
                      type="number"
                      min={1}
                      max={5}
                      value={newPostStars}
                      onChange={(e) =>
                        setNewPostStars(parseInt(e.target.value))
                      }
                    />
                  </Box>
                  <TextareaAutosize
                    placeholder="Car"
                    value={newPostCar}
                    onChange={(e) => setNewPostCar(e.target.value)}
                  />
                  <Button variant="contained" onClick={handleSubmit}>
                    Submit
                  </Button>

                  <Divider />

                  {/* Render the filtered posts */}
                  {posts.map((post) => (
                    <ListItem
                      key={post.id}
                      button
                      style={{
                        padding: "10px",
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                    >
                      <Stack style={{ overflowY: "auto" }}>
                        <Typography variant="h5" style={{ color: "green" }}>
                          {post.client.firstName + " " + post.client.lastName}
                        </Typography>
                        <Typography
                          variant="h6"
                          style={{ position: "absolute", right: "0" }}
                        >
                          {new Date(post.startDate).toLocaleString("PL", { dateStyle: "short", timeStyle: "short" })}
                        </Typography>
                        <Typography>
                          {post.content} - {post.stars} stars
                        </Typography>
                        <Typography>{post.car}</Typography>
                      </Stack>
                    </ListItem>
                  ))}
                  <Divider />
                </>
              )}

              {/* Render the book component */}
              {selectedComponent === "Book" && <BookStation />}
            </Box>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
};

export default StationView;
