import { Box, Button, Divider, Grid, Typography } from "@mui/material";
import { useState, useEffect } from "react";
import TopBar from "../components/main/TopBar";
import { useBaseReservations } from "../hooks/useReservations";
import Reservation from "../models/Reservation"; // Update with the correct path to your interfaces
import { useNavigate } from "react-router-dom";
import ReservationDTO from "models/ReservationDTO";

const Reservations = () => {
  const [reservations, setReservations] = useState<Reservation[]>([]);
  const { getReservations, updateReservation, deleteReservation } =
    useBaseReservations();
  const navigate = useNavigate();
  const [client, setClient] = useState<Client>();

  useEffect(() => {
    const fetchReservations = async () => {
      try {
        const reservationsData = await getReservations();
        const sortedReservations = reservationsData.sort((a, b) => {
          return (
            new Date(b.startDate).getTime() - new Date(a.startDate).getTime()
          );
        });
        setReservations(sortedReservations);
      } catch (error) {
        console.error("Error fetching reservations:", error);
      }
    };

    if (reservations.length === 0) {
      fetchReservations();
    }
  }, [getReservations, reservations]);

  const handleNavigation = (chargingStationId) => {
    navigate(`/station/${chargingStationId}`);
  };

  const handleConfirmation = (reservation: Reservation) => {
    // use updateReservation from useBaseReservations hook
    const updatedReservation: ReservationDTO = {
      id: reservation.id,
      startDate: reservation.startDate,
      endDate: reservation.endDate,
      chargingStationId: reservation.chargingStation.id,
      clientId: reservation.client.id,
      confirmed: true,
      active: reservation.active,
    };

    updateReservation(reservation.id, updatedReservation);
    const updatedReservations = reservations.map((res) =>
      res.id === reservation.id ? { ...res, confirmed: true } : res
    );
    setReservations(updatedReservations);
  };

  const handleCancellation = (reservationId) => {
    // use deleteReservation from useBaseReservations hook
    deleteReservation(reservationId);
    const updatedReservations = reservations.filter(
      (reservation) => reservation.id !== reservationId
    );
    setReservations(updatedReservations);
  };

  useEffect(() => {
    const storedClient = localStorage.getItem("client");
    if (storedClient) {
      const client = JSON.parse(storedClient);
      setClient(client);
    }
  }, []);

  console.log(client);

  return (
    <div>
      <TopBar />
      <Box padding={"3rem"}>
        <Grid spacing={2} height={"100%"}>
          <Grid xs={12} lg={5}>
            <Typography variant="h4" gutterBottom>
              Your reservations
            </Typography>
            {reservations.map((reservation) => (
              <Box
                flexWrap="wrap"
                key={reservation.id}
                marginBottom="1rem"
                display="flex"
                justifyContent="space-evenly"
                alignItems="center"
              >
                <Box>
                  <Typography variant="h5" style={{ color: "green" }}>
                    {reservation.chargingStation.name}
                  </Typography>
                  <Typography variant="body1">
                    {reservation.chargingStation.address.street},{" "}
                    {reservation.chargingStation.address.city}
                  </Typography>
                  <Typography variant="body1">
                    {new Date(reservation.startDate).toLocaleString()}
                  </Typography>
                  <Typography variant="body1">
                    {new Date(reservation.endDate).toLocaleString()}
                  </Typography>
                </Box>
                <Box>
                  <Typography
                    variant="body1"
                    style={{
                      color:
                        new Date(reservation.endDate) < new Date()
                          ? "darkgray"
                          : "green",
                    }}
                  >
                    {new Date(reservation.endDate) < new Date()
                      ? "Reservation Finished"
                      : "Incoming Reservation"}
                  </Typography>
                </Box>
                <Box>
                  <Typography
                    variant="body1"
                    style={{
                      color:
                        reservation.confirmed === false ? "orange" : "green",
                    }}
                  >
                    {reservation.confirmed ? "Confirmed" : "Not Confirmed Yet"}
                  </Typography>
                </Box>
                <Box>
                  <Button
                    variant="outlined"
                    onClick={() =>
                      handleNavigation(reservation.chargingStation.id)
                    }
                  >
                    Go to Station
                  </Button>
                </Box>
                <Box>
                  <Button
                    variant="outlined"
                    onClick={() => handleConfirmation(reservation)}
                  >
                    Confirm
                  </Button>
                </Box>
                <Box>
                  <Button
                    variant="outlined"
                    onClick={() => handleCancellation(reservation.id)}
                    disabled={new Date(reservation.endDate) < new Date()}
                  >
                    Cancel
                  </Button>
                </Box>
              </Box>
            ))}
          </Grid>
        </Grid>
      </Box>
    </div>
  );
};

export default Reservations;
