import { Box, Grid } from "@mui/material"
import { useGpsChargers } from "../hooks/useChargers";
import { useEffect, useState } from "react";
import CustomizedMap from "../components/map/CustomizedMap"
import SideBar from "../components/main/SideBar";
import TopBar from "../components/main/TopBar";
import GpsStation from "models/GpsStation";

const MainView = ({ position }: { position: any }) => {
  const { lat, lon } = position;
  console.log(lat, lon);
  const [chargers, setChargers] = useState<GpsStation[]>([])

  const { getGpsChargers: getChargers } = useGpsChargers();

  useEffect(() => {
    getChargers(lat, lon).then(data => setChargers(data));
  }, [lat, lon]);

  return (
    <div style={{ height: '100%' }}>
      <TopBar />
      <Box height={'auto'} bottom={'0'} top={'50px'} width={'100%'} position={'absolute'}>
        <Grid container direction="row" spacing={2} columnSpacing={{ xs: 1, sm: 2, md: 3 }} height={"100%"} padding={'1rem'}>
          <Grid item xs={12} lg={7} minHeight={'60%'}>
            <CustomizedMap chargers={chargers} />
          </Grid>
          <Grid item xs={12} lg={5}>
            <SideBar chargers={chargers} />
          </Grid>
        </Grid>
      </Box>
    </div>
  )
}

export default MainView
