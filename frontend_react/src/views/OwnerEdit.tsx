
import TopBar from "../components/main/TopBar";
import OwnerEditView from "../components/owner/OwnerEditView";

const OwnerEdit = () => {

  return (
    <div>
      <TopBar />
      <OwnerEditView />
    </div>
  )
}

export default OwnerEdit;