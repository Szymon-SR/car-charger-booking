import React, { useEffect, useState } from "react";
import {
  Typography,
  Stack,
  Button,
  TextField,
} from "@mui/material";

import { DatePicker, TimePicker } from "@mui/x-date-pickers";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import dayjs, { Dayjs } from "dayjs";
import jwt_decode from "jwt-decode";
import { useBaseClients } from "../hooks/useClients";
import Client from "models/Client";
import ReservationDTO from "models/ReservationDTO";

const BookStation = () => {
  const [startDate, setStartDate] = useState<Date | null>(null);
  const [startTime, setStartTime] = useState<Date | null>(null);
  const [endDate, setEndDate] = useState<Date | null>(null);
  const [endTime, setEndTime] = useState<Date | null>(null);
  const [clientName, setClientName] = useState("");
  const [reservationSubmitted, setReservationSubmitted] = useState(false);

  const token = localStorage.getItem("token");
  // decode token
  const decoded: any = token ? jwt_decode(token) : null;
  const email = decoded ? decoded.email : null;

  console.log(email);
  const [client, setClient] = useState<Client | undefined>();

  const { getClientByEmail } = useBaseClients();

  useEffect(() => {
    if (email) {
      getClientByEmail(email).then((data) => setClient(data));
    }
  }, [email]);

  const handleStartDateChange = (date: Date | null) => {
    setStartDate(date);
  };

  const handleStartTimeChange = (date: Date | null) => {
    setStartTime(date);
  };

  const handleEndDateChange = (date: Date | null) => {
    setEndDate(date);
  };

  const handleEndTimeChange = (date: Date | null) => {
    setEndTime(date);
  };

  const handleClientNameChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setClientName(event.target.value);
  };

  const handleSubmit = (event: React.FormEvent) => {
    event.preventDefault();
    // create a new reservationDTO object
    const reservation: ReservationDTO = {
      chargingStationId: 6, // change this later
      clientId: client?.id,
      startDate: combineDateTime(startDate!, startTime!),
      endDate: combineDateTime(endDate!, endTime!),
      confirmed: false,
      active: true,
    };

    console.log(reservation);
    setStartDate(null);
    setStartTime(null);
    setEndDate(null);
    setEndTime(null);
    setClientName("");
    setReservationSubmitted(true);
  };

  //TODO FIX
  // Function to combine date and time into a single Date object
  const combineDateTime = (date: Date, time: Date) => {
    const combinedDate = new Date(
      date.getFullYear(),
      date.getMonth(),
      date.getDate(),
      time.getHours(),
      time.getMinutes()
    );
    return combinedDate;
  };

  //console log comnibined data from date and time pickers
  useEffect(
    () => {
      if (startDate && startTime) {
        console.log({ startDate });
        console.log({ startTime });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [startDate, startTime]
  );

  return (
    <div>
      {reservationSubmitted ? (
        <Typography variant="h6" sx={{ m: 2 }}>
          Reservation submitted successfully!
        </Typography>
      ) : (
        <form onSubmit={handleSubmit}>
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <Stack direction="column" spacing={2} sx={{ m: 2 }}>
              <DatePicker
                label="Start Date"
                value={startDate}
                onChange={handleStartDateChange}
                renderInput={(params) => <TextField {...params} />}
              />
              <TimePicker
                label="Start Time"
                value={startTime}
                onChange={handleStartTimeChange}
                renderInput={(params) => <TextField {...params} />}
              />
              <DatePicker
                label="End Date"
                value={endDate}
                onChange={handleEndDateChange}
                renderInput={(params) => <TextField {...params} />}
              />
              <TimePicker
                label="End Time"
                value={endTime}
                onChange={handleEndTimeChange}
                renderInput={(params) => <TextField {...params} />}
              />
              <TextField
                label="Client Name"
                value={clientName}
                onChange={handleClientNameChange}
              />
              <Button type="submit" variant="contained">
                Submit Reservation
              </Button>
            </Stack>
          </LocalizationProvider>
        </form>
      )}
    </div>
  );
};

export default BookStation;
