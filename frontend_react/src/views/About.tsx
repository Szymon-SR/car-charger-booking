import { Typography, Box } from '@mui/material';

import TopBar from "../components/main/TopBar";


const MainView = () => {
  return (
    <div>
      <TopBar />
      <Box sx={{width: 1/2, justifyContent: 'center', position: 'absolute', top: '50px'}}>
        <Typography variant='h3' sx={{ m: 2 }}>About website</Typography>
        <Typography sx={{ m: 2 }}>
          Our website is an interactive map that displays the locations of electric car chargers in a particular area. Users can search for charging
          stations based on their location and filter the results by charger type, availability, and charging speed. Additionally, users can rate the charging
          stations based on their experience, which helps other users decide which charging station to use.
        </Typography>
        <Typography sx={{ m: 2 }}>
          The website is designed to be user-friendly, with an intuitive interface that allows users to easily navigate the map and find the information
          they need. The website also provides detailed information about each charging station, such as the type of connector, pricing, and any special
          features like access to restrooms or nearby restaurants.
        </Typography>
        <Typography sx={{ m: 2 }}>
          Overall, our website provides a valuable service to electric car owners by helping them find reliable and convenient charging stations, while
          also allowing them to share their experiences with other users. This can help to promote the use of electric cars and encourage more people
          to make the switch to sustainable transportation.
        </Typography>
      </Box>

    </div>
  )
}

export default MainView
