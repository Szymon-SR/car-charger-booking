import { Typography, Box, Stack } from '@mui/material';
import Grid from '@mui/material/Unstable_Grid2'; // Grid version 2
import TopBar from "../components/main/TopBar";

import OwnerDetail from '../components/owner/OwnerDetail';

const OwnerView = () => {

  return (
    <div>
      <TopBar />
      <OwnerDetail />
    </div>
  )
}

export default OwnerView;